
## This template is for filing a bug report

This repository primarily relates to the logged out [marketing website](https://about.gitlab.com/) and [company handbook](https://about.gitlab.com/handbook/). This is not the right repository for requests related to docs.gitlab.com, product, or other parts of the site.

## Please delete the lines above and any irrelevant sections below.

Below are suggestions. Any extra information you provide is beneficial.

#### What is/are the relevant URLs or emails?

(Example: https://about.gitlab.com/something/etc/)

#### Briefly describe the bug

(Example: A link is broken and when hovering over it the color is the wrong color and poorly aligned.)

#### What are the steps to reproduce the bug

(Example (In a bulleted list):)

* Scroll to the W section of the page
* When condition X is true
* Hover over Y

#### Please provide any relevant screenshots or screencasts

(Example: Insert screenshot(s) here)

* [How to take a screenshot](https://www.howtogeek.com/205375/how-to-take-screenshots-on-almost-any-device/)

#### What is your window size, browser, operating system, and versions

(Example: Chrome 77, OSX 10.15.0, 944px by 977px. [https://whatsmybrowser.org/b/MPC757K](https://whatsmybrowser.org/b/MPC757K))

[https://www.whatsmybrowser.org](https://www.whatsmybrowser.org/)

#### What device are you using

(Example: Samsung Galaxy S 10 Plus)

#### Have you tried an incognito window? Could this be related to cookies or account type/state?

(Examples: I tried incognito, no impact OR Problem goes away incognito OR only happens when YYYYYY cookies are set to ZZZZZZ OR logged in as permissioned user.)

#### If applicable, what browser plugins might be related to the issue

(Example: Adblock, noscript, safe browsing)

#### If applicable, what is your geographic location

(Example: Eurpoean Union is relevant to GDPR or cookie notices)

#### What type of network is this? Could any corporate firewalls be impacting this? Have you contacted your network administrator if applicable?

(Example1: Home network, charter cable, D-Link wi-fi router model #### )

(Example2: Corporate network, [Company/Govt/Entity/etc name, ex: Adidas] bitdefender box firewall, wired connection, network administrator not contacted )

<!-- do not edit below this line-->

/label ~"mktg-website" ~"template::web-bug" ~"mktg-status::triage"
/cc @gl-website
