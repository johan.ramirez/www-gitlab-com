require 'yaml'

def check_file
  role_levels = YAML.load_file('data/role_levels.yml')

  # check if there's always a `is_default` set
  role_levels.each do |role_title, levels|
    send_default_missing_error(role_title) if levels.none? { |level| level['is_default'] }
  end

  # check if the job_families always refer to an existing level
  job_families = YAML.load_file('data/job_families.yml')
  all_levels = role_levels.keys
  job_families.each do |job_family|
    send_level_is_missing_error(job_family) unless all_levels.include? job_family['levels']
  end
end

def send_default_missing_error(role_title)
  raise "In role_levels.yml: `is_default` is missing for #{role_title}."
end

def send_level_is_missing_error(job_family)
  raise "In job_families.yml: the level #{job_family['levels']} for #{job_family['title']} does not exist in `role_levels.yml`."
end

p "Checking role_levels.yml and job_families file because changes have been detected to one or both of those files."
check_file
p "Done checking role_levels.yml file"
