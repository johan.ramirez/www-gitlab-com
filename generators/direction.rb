require_relative '../lib/gitlab/file_cache'
require 'active_support/core_ext/date/calculations'
require 'active_support/core_ext/numeric/time'
require 'active_support/notifications'

require 'faraday_middleware'
require 'faraday_middleware/parse_oj'

module Generators
  class Direction
    module GitlabParamsEncoder
      extend self

      def encode(hash)
        # The GitLab API does _not_ handle encoded `+` properly in label names
        Faraday::NestedParamsEncoder.encode(hash).gsub('%2B', '+')
      end

      def decode(string)
        Faraday::NestedParamsEncoder.decode(string)
      end
    end

    class GitLabInstance
      include ::Gitlab::FileCache

      set_cache_store 'direction'

      attr_reader :name

      HTTP_OK = 200

      def initialize(name)
        @connection = Faraday.new(
          url: endpoint,
          headers: headers,
          request: { params_encoder: GitlabParamsEncoder, timeout: 90 }
        ) do |config|
          config.request :json
          config.response :logger, nil, { headers: false, bodies: false }
          config.response :oj
          config.adapter Faraday.default_adapter
        end

        @name = name

        cache_store.clear if ENV.key?('CLEAR_DIRECTION_CACHE')
      end

      def get_single(path, params = {})
        cache_key = @connection.build_url(path, params).to_s

        cached(cache_key) do
          begin
            retry_count ||= 0
            response = @connection.get(path, params)

            if response.status != HTTP_OK
              warn "Error in retrieving URL #{response.env.url}: #{response.status}"
              abort_cache!([])
            end

            response
          rescue Faraday::ClientError, Faraday::ParsingError, Faraday::ConnectionFailed => ex
            # retry on intermittent network errors (429s can appear here as parsing errors)
            warn "Error in retrieving URL #{ex.inspect}, retrying #{retry_count}/5"
            sleep(10)
            retry if (retry_count += 1) < 5
            abort_cache!([])
          end
        end
      end

      def get(path, additional_pages, params = {})
        content = []
        page = 1

        loop do
          params['page'] = page
          response = get_single(path, params)
          if Hash === response.body
            warn "Hash returned by #{response.env.url}"
            return response.body
          else
            content += response.body
          end
          page += 1
          break if page > response.headers['X-Total-Pages'].to_i || !additional_pages
        end
        content
      end

      private

      def endpoint
        'https://gitlab.com/api/v4'
      end

      def headers
        {
          'PRIVATE-TOKEN' => ENV['PRIVATE_TOKEN'],
          'User-Agent' => 'www-gitlab-com'
        }
      end
    end

    class GitLabProject
      def initialize(id, instance)
        @id = id.gsub('/', '%2F')
        @instance = instance
        @name = instance.name
      end

      def milestones(state = 'active')
        result = @instance.get("projects/#{@id}/milestones", true, state: state, per_page: 100)
        result = result.select { |ms| ms['due_date'] }
        result.sort_by! do |ms|
          Date.parse ms['due_date']
        end
      end

      def milestone(milestone_id)
        @instance.get("projects/#{@id}/milestones/#{milestone_id}", true, per_page: 100)
      end

      def milestone_direction_issues(milestone_id, labels)
        result = []
        labels.each do |label|
          result += @instance.get("projects/#{@id}/issues", true, milestone: milestone_id, labels: 'direction,' + label, per_page: 100)
        end
        result
      end

      def wishlist_issues(label, not_label = nil)
        result = @instance.get("projects/#{@id}/issues", true, milestone: 'No+Milestone', labels: "direction,#{label}", state: 'opened', per_page: 100, sort: 'asc')
        result += @instance.get("projects/#{@id}/issues", true, milestone: 'Backlog', labels: "direction,#{label}", state: 'opened', per_page: 100, sort: 'asc')
        result = result.select { |issue| (issue['labels'] & not_label).empty? } if not_label
        result
      end

      def tier_issues(tier)
        @instance.get("projects/#{@id}/issues", true, labels: "direction,#{tier}", state: 'opened', per_page: 100, sort: 'asc')
        # result = result.select { |issue| issue["milestone"] && issue["milestone"]["due_date"] }
      end

      def name
        project['name']
      end

      def web_url
        project['web_url']
      end

      def project
        @project ||= @instance.get("projects/#{@id}", true, per_page: 100)
      end
    end

    class GitLabGroup
      def initialize(id, instance)
        @id = id
        @instance = instance
        @name = instance.name
      end

      def milestones(state = 'active')
        result = @instance.get("groups/#{@id}/milestones", true, state: state, per_page: 100)
        result = result.select { |ms| ms['due_date'] }
        result.sort_by! do |ms|
          Date.parse ms['due_date']
        end
      end

      def merge_request_count(stage, milestone, labels = nil, state = 'merged')
        label_set = labels ? "devops::#{stage}, #{labels}" : "devops::#{stage}"
        result = @instance.get("groups/#{@id}/merge_requests", true, milestone: milestone, labels: label_set, state: 'merged', per_page: 100, sort: 'asc')
        result
      end

      def group
        @group ||= @instance.get("groups/#{@id}", true, per_page: 100)
      end
    end

    STAGES = %w[manage plan create verify package release configure monitor secure defend enablement].freeze
    DEV_STAGES = %w[manage plan create].freeze
    CICD_STAGES = %w[verify package release].freeze
    SEC_STAGES = %w[secure].freeze
    OPS_STAGES = %w[configure monitor].freeze
    DEFEND_STAGES = %w[defend].freeze
    ENABLEMENT_STAGES = %w[enablement].freeze

    def generate
      raise 'PRIVATE_TOKEN required to generate direction page' unless ENV['PRIVATE_TOKEN']

      generate_direction(DEV_STAGES)
      generate_direction(CICD_STAGES)
      generate_direction(SEC_STAGES)
      generate_direction(OPS_STAGES)
      generate_direction(DEFEND_STAGES)
      generate_direction(STAGES)
      generate_wishlist
      generate_product_vision
    end

    def get_direction_issues(project, milestone, stages)
      # some projects only contain issues from one stage
      issues = []
      case project.name
      when "gitlab-pages" # devops::release
        issues += project.milestone_direction_issues(milestone['title'], ["devops::release"]) if stages.include? "release"
      when "gitlab-runner" # devops::verify
        issues += project.milestone_direction_issues(milestone['title'], ["devops::verify"]) if stages.include? "verify"
      when "omnibus-gitlab" # devops::enablement
        issues += project.milestone_direction_issues(milestone['title'], ["devops::enablement"]) if stages.include? "enablement"
      else
        # for other projects, check all labels
        issues += project.milestone_direction_issues(milestone['title'], stages.map { |n| "devops::" + n })
      end
      issues
    end

    def format_direction_header(title, due_date)
      # check if the release number can cast to float (i.e., 11.1, 11.12, etc.)
      # this will be used to not display a date for non-matching, but included
      # milestones such as "Next 3-4 releases"
      begin
        if Float(title)
          # was able to cast to float, looks like a normal release number
          # milestone due date is actually code freeze, so we need to add 5 days so we display the correct date
          display_date = Date.parse(due_date) + 5.days
          output = "### #{title} (" + display_date.strftime('%Y-%m-%d') + ")\n\n"
        end
      rescue ArgumentError
        # milestone title is not a normal release number, do not include extra date info
        output = "### #{title}\n\n"
      end
      output
    end

    def get_issues(edition, milestone, stages)
      issues = []
      edition.each do |project|
        issues += get_direction_issues(project, milestone, stages)
      end
      issues
    end

    def get_sorted_milestones(milestones)
      milestones.sort_by! do |ms|
        Date.parse ms['due_date']
      end
      milestones
    end

    def get_direction_buckets(issues, stages)
      buckets = {}
      issues.each do |issue|
        stages.each do |stage|
          buckets[stage] = [] if buckets[stage].nil?
          buckets[stage] << issue if issue['labels'].include?("devops::#{stage}")
        end
      end
      buckets
    end

    def generate_direction(stages)
      # stages contains list like: stages = %w[manage plan create verify package release configure monitor secure]

      direction_output = Hash.new { |hash, key| hash[key] = Hash.new(&hash.default_proc) }
      direction_output["all"]["all"] = ''

      get_sorted_milestones(gitlaborg.milestones).each do |ms|
        # subtract 5 days because the release's milestone due date is set to the freeze, not release date
        next unless ms['due_date'] && Date.parse(ms['due_date']) >= Date.today - 5.days

        issues = get_issues(edition, ms, stages)

        next if issues.empty?

        direction_output["all"]["all"] << format_direction_header(ms['title'], ms['due_date'])

        # run through all issues and throw in relevant bucket (stage)
        buckets = get_direction_buckets(issues, stages)

        # Now unfold the bins in order, so all the issues are under their stage
        stages.each do |stage|
          next unless buckets[stage].count.positive?

          direction_output[ms['title']][stage] = ''

          # If there is only one potential stage to include, the stage name is not necessary to enumerate.
          direction_output["all"]["all"] << "#### #{stage.capitalize}\n" if stages.length > 1
          buckets[stage].each do |issue|
            direction_output[ms['title']][stage] << issue_bullet(issue)
          end
          direction_output[ms['title']][stage] << "\n"
          direction_output["all"]["all"] << direction_output[ms['title']][stage] + "\n"
        end
      end
      direction_output
    end

    def generate_wishlist
      puts 'Generating wishlist...'
      output = {}

      # 'boards',
      # 'burndown charts',
      # 'capacity planning',
      # 'chat commands',
      # 'code review',
      # 'container registry',
      # 'deliver',
      # 'epics',
      # 'issue boards',
      # 'issues',
      # 'jira',
      # 'labels',
      # 'milestones',
      # 'major wins',
      # 'moderation',
      # 'notifications',
      # 'open source',
      # 'performance',
      # 'roadmaps',
      # 'search',
      # 'Monitoring',
      # 'search',
      # 'service desk',
      # 'todos',
      # 'usability',
      # 'vcs for everything',
      # 'wiki',

      [
        'devops::manage',
        'devops::plan',
        'devops::create',
        'devops::verify',
        'devops::package',
        'devops::release',
        'devops::configure',
        'devops::monitor',
        'devops::secure',
        'HA',
        'Cloud Native',
        'moonshots'
      ].each do |label|
        output[label] = label_list(label)
      end
      ['GitLab Starter', 'GitLab Premium', 'GitLab Ultimate'].each do |tier|
        output[tier] = tier_list(tier)
      end

      puts

      output
    end

    def generate_contribution_count(stages)
      puts 'Generating contribution counts...'

      milestones = gitlaborg.milestones('closed').select { |m| m['title'].match(/\d+\.\d+/) }
      milestones = milestones.reverse

      gitlaborg_ref = gitlaborg # Required to store a local copy since Middleman changes self when doing lazyloading of stages

      stage_contributions = {}
      stages.stages.each do |stagekey, stage|
        count = 0
        milestones.first(3).each do |m|
          contributed_mrs = gitlaborg_ref.merge_request_count(stagekey, m['title'], "Community contribution")
          count += contributed_mrs.count()
          puts "Contributions for #{m['title']}: #{contributed_mrs.count}"
        end

        stage_contributions[stagekey] = count
      end

      stage_contributions
    end

    def generate_stage_velocity(stages)
      puts 'Generating contribution counts...'

      milestones = gitlaborg.milestones('closed').select { |m| m['title'].match(/\d+\.\d+/) }
      milestones = milestones.reverse

      gitlaborg_ref = gitlaborg # Required to store a local copy since Middleman changes self when doing lazyloading of stages

      stage_velocity = {}
      stages.stages.each do |stagekey, stage|
        count = 0
        milestones.first(3).each do |m|
          merged_mrs = gitlaborg_ref.merge_request_count(stagekey, m['title'])
          count += merged_mrs.count()
          puts "Merged MRs for #{m['title']}: #{stage_velocity.count}"
        end

        stage_velocity[stagekey] = count
      end

      stage_velocity
    end

    def generate_milestones
      milestones = {}
      milestones['last'] = gitlaborg.milestones('closed').select { |m| m['title'].match(/\d+\.\d+/) }.reverse.first['title']
      milestones['next'] = gitlaborg.milestones('active').select { |m| m['title'].match(/\d+\.\d+/) }.first['title']

      milestones
    end

    private

    def issue_bullet(issue)
      output = "- [#{issue['title']}](#{issue['web_url']})"
      output << ' <kbd>Starter</kbd>' if issue['labels'].include? 'GitLab Starter'
      output << ' <kbd>Premium</kbd>' if issue['labels'].include? 'GitLab Premium'
      output << ' <kbd>Ultimate</kbd>' if issue['labels'].include? 'GitLab Ultimate'
      output << ' <kbd>Kickoff Highlight</kbd>' if issue['labels'].include? 'Kickoff Highlight'
      output << "\n"
      output
    end

    def tier_bullet(issue)
      output = "- [#{issue['title']}](#{issue['web_url']})"
      output << " <kbd>#{issue['milestone']['title']}</kbd>" if issue['milestone']
      output << "\n"
      output
    end

    def epic_web_url(group, epic)
      "#{group.group['web_url']}/-/epics/#{epic['iid']}"
    end

    def edition
      @edition ||= begin
        com = GitLabInstance.new('GitLab.com')
        gitlab = GitLabProject.new('gitlab-org/gitlab', com)
        omnibus = GitLabProject.new('gitlab-org/omnibus-gitlab', com)
        gitlab_chart = GitLabProject.new('gitlab-org/charts/gitlab', com)
        runner_chart = GitLabProject.new('gitlab-org/charts/gitlab-runner', com)
        runner = GitLabProject.new('gitlab-org/gitlab-runner', com)
        pages = GitLabProject.new('gitlab-org/gitlab-pages', com)
        [gitlab, omnibus, gitlab_chart, runner_chart, runner, pages]
      end
    end

    def gitlaborg
      @gitlaborg ||= begin
        com = GitLabInstance.new('GitLab.com')
        GitLabGroup.new('gitlab-org', com)
      end
    end

    def label_list(label, exclude: nil, editions: nil)
      output = ''

      editions = edition if editions.nil?

      editions.each do |project|
        issues = project.wishlist_issues(label, exclude)
        issues.each do |issue|
          output << issue_bullet(issue)
        end
      end
      output = "No current issues\n" if output.empty?
      output
    end

    def tier_list(label)
      output = ''
      issues = []

      edition.each do |project|
        issues += project.tier_issues(label)
      end
      issues.sort_by! do |issue|
        if issue.dig('milestone', 'due_date')
          Date.parse(issue['milestone']['due_date'])
        else
          Date.new(2050, 1, 1)
        end
      end
      issues.each do |issue|
        output << tier_bullet(issue)
      end

      output = "No current issues\n" if output.empty?
      output
    end
  end
end
