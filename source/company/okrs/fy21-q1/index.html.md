---
layout: markdown_page
title: "FY21-Q1 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2020 to April 30, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO Objective: IACV
1. CEO KR: End Q1 with Q2 New business IACV Stage 3+ [Pipeline](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) and Growth business IACV Stage 3+ Pipeline both ahead of plan.
1. CEO KR: Maintain quota capacity at least 5% ahead of plan for FY21.  Exceed Q1 [NQR](/handbook/sales/commissions/#quotas-overview) [hiring](/handbook/hiring/charts/sales-nqr/)) by at least 5% quota coverage.
   1. **CRO: Hire NQRs to Q1 plan**
      1. KR: NQR hire start dates in Q1 exceed plan
      1. KR: Recruiting program at least 5% above capacity to hit plan
      1. KR: Overlay hires with start dates in Q1 at or above plan 
1. CEO KR: Renewal process satisfaction, as measured by [SSAT](/handbook/business-ops/data-team/kpi-index/#satisfaction) for tickets about renewals, is greater than 95%.
   1. **Director of Product, Growth: Improve the customer experience with billing related workflows.**
      1. Director of Product, Growth KR: Make material improvements to direct signup, trial, seat true-up, upgrade, and renewal workflows, both for .com and self-hosted customers.
      1. Director of Product, Growth KR: Drive [Support Satisfaction](/handbook/support/performance-indicators/#support-satisfaction-ssat) scores, filtered for billing, to >95%.
      1. Director of Product, Growth KR: Drive [$561k in incremental IACV](https://gitlab.com/gitlab-org/growth/product/issues/805).
1. **VP of Product Management: Complete pricing analysis and, if required, roll out related updates to GitLab's pricing and packaging.**
   1. VP of Product Management KR: Complete pricing analysis project and, if required, implement at least the first phase of any recommended changes.
1. **CRO: Launch New Channel Program** 
   1. CRO KR: New tiers, terms and agreement complete, published on partner portal and handbook for all partners
   1. CRO KR: Automated deal reg launched and available in portal 
   1. CRO KR: Deliver at least one new PIO in every region through portal
1. **CRO: Increase stage usage by at least one stage for 10 of top 50 accounts with only 1-2 stages in use as measured by SMAU >10% per stage.** 
   1. CRO KR: Targeted account based plan to offer services and support to utilize a new stage at each of the 50 targeted accounts.
   1. CRO KR: 10 accounts add at least one stage with SMAU moving from <5% to >25% in a specific stage.

### 2. CEO: Popular next generation product
1. CEO KR: [SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau) is being measured and reported in 100% of Sales and Product [Key Meetings](/handbook/finance/key-meetings/).
   1. **Director of Product, Growth:  Ensure accurate data collection and reporting of AMAU and SMAU metrics.**
      1. Director of Product, Growth KR: Deliver AMAU and SMAU tracking with less than 2% uncertainty.
1. CEO KR: [SPU](/handbook/product/metrics/#stages-per-user) increases by 0.25 stages from EOQ4 to EOQ1.
1. CEO KR: [MAU](/handbook/product/metrics/#monthly-active-users-mau) increases 5% from EOQ4 to EOQ1.
1. **VP of Product Management: Proactively validate problems and solutions with customers.**
   1. VP of Product Management KR: At least 2 validation cycles completed per Product Manager.
1. **VP of Product Management: Create demos of the top competitor in each stage to compare against our own demos completed in Q4.**
   1. VP of Product Management KR: Deliver one recorded demo for each stage.
1. **Principal Product Manager, Product Operations: Roll out [Net Promoter Score (NPS)](/handbook/product/metrics/#paid-net-promoter-score) tracking.**
   1. Principal Product Manager, Product Operations KR: Survey at least 25% of GitLab's paid customer base, with a reponse rate of >4%.
1. **Director, Product, Ops:  Work with GitLab's infrastructure team to dogfood our APM metrics.**
   1. Director, Product, Ops KR:  Ensure APM metric dogfooding is properly prioritized.
1. **VP of Product Strategy: Get strategic thinking into the org.** 
    1. VP of Product Strategy KR: Secure and Enablement section strategy reviews
    1. VP of Product Strategy KR: Produce [strategy visuals](https://gitlab.com/gitlab-com/Product/issues/512)
1. **VP of Product Strategy: Lay groundwork for strategic initiatives.** 
    1. VP of Product Strategy KR: Hire strategic initiatives team
1. **Sr. Director of Corp Dev: Get acquisitions into shape; build a well-oiled machine.** 
    1. Sr. Director of Corp Dev KR: Identify 1000 [qualified acquisition targets](/handbook/acquisitions/performance-indicators/#qualified-acquisition-targets).

### 3. CEO: Great team
1. CEO KR: Teams are working [handbook-first](/handbook/handbook-usage/#why-handbook-first) and no content is being produced in other tools, e.g. Docs, Classroom, etc.
1. CEO KR: There 7 certifications online for all community members to pursue.
1. CEO KR: There are 200 certifications completed by [middle managers](/company/team/structure/#middle-management) at GitLab.

## How to Achieve Presentations
