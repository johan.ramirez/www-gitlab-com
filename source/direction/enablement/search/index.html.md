---
layout: markdown_page
title: "Category Direction - Search"
---

- TOC
{:toc}

## Search

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thanks for visiting this direction page on Search in GitLab. This page belongs to the [Search](/handbook/product/categories/#search-group) group of the Enablement stage and is maintained by Kai Armstrong([E-Mail](mailto:karmstrong@gitlab.com)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=search) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=search) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy.
 - Please share feedback directly by submitting a Merge Request to this page. Alternatively, feel free to reach out via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for search, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is. If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it. Please include use cases, personas, and user journeys into this section. -->

GitLab currently supports [Advanced Global Search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html) for Self-Managed instances. This provides users with a faster and more complete search across the GitLab instance and within the code. Unfortunately, we haven't been able to bring these features to GitLab.com users. Our goal is to systematically roll out Advanced Global Search, beginning with the GitLab.org Group and then to the rest of GitLab.com.

One of the primary benefits of continuing to expand our Advanced Global Search to Gitlab.com is the ability to provide cross project code search to users of GitLab.com. By enabling cross project searching users will be better equipped to discover solutions in other parts of their own organization that might be solving or thinking about the same things they are.

We've also written a [blog post](https://about.gitlab.com/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/) on the lessons from our journey as we continue to work towards enabling Elasticsearch on GitLab.com.

<!-- ### Target Audience and Experience -->
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.-->

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top priorities for a few stakeholders. This section must provide a link to an issue or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

**In Progress: [Elasticsearch admin controls](https://gitlab.com/groups/gitlab-org/-/epics/428)** - After Elasticsearch has been enabled for the [GitLab.org Group](https://gitlab.com/gitlab-org) we need to continue improving administrative control of the Elasticsearch index and related tasks. These are important for GitLab to manage the large scale of the deployment for GitLab.com and for administrators working in large self-managed instances.

**In Progress: [Enable Elasticsearch for Paid Groups on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/1736)** - Now that Elasticsearch has been enabled for the `gitlab-org` group on GitLab.com we're focusing on how to continue enabling Elasticsearch for Paid groups on GitLab.com. This is an important next step in expanding the scale of our Elasticsearch deployment and further support our [dogfooding](https://about.gitlab.com/handbook/product/#dogfood-everything) principles. 

**Next: [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)** - Once we've enabled Elasticsearch for Paid Groups on GitLab.com we'll continue to work on bringing these features to the rest of GitLab.com. Making code search work is an important feature and we understand the need and value this will provide.

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk about, but we’re talking with customers about what they actually use, and ultimately what they need.-->

Both GitHub and BitBucket provide a more comprehensive and complete search for users; particularly in their ability to deeply search code and surface those results to users. While GitLab's Advanced Global Search is available to self-managed users it hasn't reached all users across GitLab.com.

<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most thumbs-up), but you may have a different item coming out of customer calls.-->

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)

### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)
