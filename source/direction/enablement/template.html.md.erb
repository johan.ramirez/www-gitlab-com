---
layout: markdown_page
title: "Product Section Direction - Enablement"
---

{:.no_toc}

- TOC
{:toc}

## Enablement section overview

The Enablement section is responsible for the features and tools our customers use to operate GitLab at all scales. Enablement supports customers from initial deployment of GitLab to ongoing operation, as well as integration of other services in use by an organization.

The Enablement section is made up of one eponymous non-DevOps stage, Enablement, and seven groups:

* [Distribution](#distribution) - Installation, upgrades, maintenance
* [Geo](#geo) - Disaster recovery and worldwide performance
* [Ecosystem](#ecosystem) - Integration with other services
* [Search](#search) - Global search and ElasticSearch integration
* [Memory](#memory) - Performance optimizations, reliability improvements, and best practices
* [Database](#database) - Database architecture and best practices
* [Infrastructure](#infrastructure) - Operating the largest GitLab instance, GitLab.com

### Mission

> Provide users with a consistently great experience and achieve customer business requirements by making GitLab easy to deploy, operate, scale, and integrate.

### Addressable market

There is no traditional addressable market for Enablement due to its foundational, GitLab-specific nature. Every GitLab user is directly impacted, however, by the work Enablement delivers.

In light of this, we think of Enablement's addressable market as that of GitLab's larger addressable market. By working to ensure we can meet the operational, compliance, and integration requirements of GitLab's customers, we can capture increasingly larger percentages of GitLab's total addressable market opportunity. We presently don't capture reasons for loss with sufficient granularity to determine when an Enablement related concern was the primary driver, making the determination of the precise missed opportunity challenging.

There are two primary segments within the broader "GitLab" market: organizations that would like to operate their own GitLab instances (self-managed) and those who prefer to utilize a SaaS service (GitLab.com). It is important to note that GitLab.com utilizes the same code base and release artifacts as our other customers, and we [document any non-default configurations](https://docs.gitlab.com/ee/user/gitlab_com/), it is simply the largest deployment of GitLab.

#### Self-managed

Today, we are able to capture most of the self-managed segment with our mature [linux packages](https://docs.gitlab.com/omnibus/README.html) and more recently our [Kubernetes Helm chart](https://docs.gitlab.com/charts/). A proof point is GitLab's [two-thirds market share](https://about.gitlab.com/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market) in the self-managed Git market. While this speaks to the competitiveness of GitLab's overall product, a critical enabling factor is the high-quality, flexible, and scalable software and operational tools.

While we are able to meet the requirements of most organizations, there are some occasional unmet needs:

* Unsupported deployment targets, such as [OpenShift](https://gitlab.com/groups/gitlab-org/-/epics/2068) or [ARM64](https://gitlab.com/groups/gitlab-org/-/epics/2370)
* Unable or infeasible to achieve the most demanding [service level objectives](https://en.wikipedia.org/wiki/Service-level_objective)
* Effort required to deploy and operate is higher than desired, typically resulting in the adoption of a SaaS service or lighter-weight solution

#### SaaS

GitLab's market share in the SaaS segment is significantly smaller than self-managed at approximately [8%](https://blog.bitrise.io/state-of-app-development-in-2018#git-hosting-in-the-cloud). While some of the disparity is due to the network effects of GitHub.com's strong open-source community, Bitbucket's 30% market share illustrates the significant upside present in this segment for us.

There are a larger number of unmet needs on GitLab.com than self-managed, which primarily fall into two buckets:

* Service level indicators, primarily [performance](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-performance) and [availability](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability), below our typical enterprise self-managed installation
* Product feature gaps compared to self-managed, like access to comprehensive audit logs, due to tenants being limited to group management

From an Enablement perspective, much of our work is on improving the performance and reliability of GitLab.com, although there are some feature gaps we are working to address, like [enablement of Advanced Global Search](https://gitlab.com/groups/gitlab-org/-/epics/153).

### Resourcing and investment

The existing team members and open vacancies for the Enablment section can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=enablement-section)
* [User Experience](https://about.gitlab.com/company/team/?department=enablement-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=enablement-pm-team)

Historical staffing levels for Enablement can be found in our [hiring charts](https://about.gitlab.com/handbook/hiring/charts/enablement-section/).

### Adoption

Due to the GitLab-specific nature of Enablement, adoption is primarily meaningful when seen as a percentage of GitLab instances. 

> **Note**: Links are to internal Periscope dashboards. Additional details can be found on internal [Enablement topic](https://app.periscopedata.com/app/gitlab/topic/Enablement/aba6a85dc0b74491b61f433a0c201282).

* [Installation types](https://app.periscopedata.com/app/gitlab/441909/Active-Instances): The Omnibus package and container image represent about 90% of all instances, with Source making up 8% and the cloud-native Kubernetes Helm chart 1%.
* [Upgrade Rate](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate): 12% of instances are running a version less than 1 month old, 45% less than 2 months, and 51% less than 3 months. Note that the second and third month are inclusive.
* [Geo]((https://app.periscopedata.com/app/gitlab/500159/WIP:-Geo)): Enabled on less than 1% of GitLab instances.

### Competitive space and position

As [noted above](#addressable-market), GitLab's competitive position is a tale of two cities. We are the leading self-managed Git solution but are a distant third in SaaS. Our success in self-managed has driven the majority of the company's growth, however it is at risk of being disrupted by growing trends in the market.

The [IDC DevOps 2019](https://www.idc.com/getdoc.jsp?containerId=US45688619) report is illustrative of this challenge, with the top two preferences for new IT infrastructure projects being community-supported open source software (OSS) and SaaS, respectively. Commercially supported OSS is fourth with under 15% share. While we have been successfully managing the open-core nature of GitLab, we are at risk of being disrupted from below by other OSS projects; for example, ones that may be lighter-weight and more focused in specific stages. GitLab.com, our SaaS service, represents both our biggest opportunity and risk depending on our execution. 

We need to achieve what could be considered opposing goals: making GitLab efficient and easy to run at [small scales](https://docs.gitlab.com/omnibus/settings/rpi.html) and improving the scalability and reliability at web-scale.

## Challenges

* Due to much of GitLab's code being written by other groups, Enablement relies largely on influence by defining best practices, policies, and frameworks to achieve stability and performance goals
* Kubernetes "package" tools are immature and a moving target
* Rapidly increasing feature surface area, driving increased demand for compute resources and operational complexity
* Breadth of deployment targets (bare metal, VM, container, Kubernetes, cloud-specific, etc.) and configuration matrixes

## 3-year strategy

In three years we expect:

* SaaS to be the preferred delivery mechanism for IT services
* DevOps tools to be business-critical applications with little to zero appetite for disruption
* Kubernetes adoption to continue to accelerate for IT services, including GitLab
* Continued improvement of open source Git projects like [gitea](https://gitea.io/en-us/) and [gogs](https://gogs.io)
* DevOps tools like GitLab to become deeply interwoven into organizational workflows and processes

As a result, in three years, GitLab will:

* Generate more incremental ARR on GitLab.com than self-managed
* Demonstrate market-leading service availability for both GitLab.com and self-managed
* Set the bar for IT apps delivered on Kubernetes
* Enable users to get started with self-managed GitLab in under 10 minutes at both 5 and 50,000 user scales
* Enable contributors, partners, and customers to easily integrate GitLab with other services

## Themes

### GitLab is easy to deploy and operate

Deploying and maintaining GitLab should be as frictionless as possible for organizations of all sizes. This is critical for GitLab at multiple points in the customer journey. 

GitLab starts as a personal or side project at many organizations, representing an important driver of initial adoption and awareness. Delighting future champions by providing a thoughtfully designed out-of-the-box experience that runs well on hardware they have lying around pays dividends in future growth.

Once a company is ready to adopt GitLab enterprise wide, it is similarly important to ensure the GitLab instance is set up for success with minimal effort. Consider our [5,000-user reference architecture](https://docs.gitlab.com/ee/administration/high_availability/#5000-user-configuration) which recommends setting up at minimum 27 different instances, and that our [GitLab configuration file is over 2,000 lines long](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template). This is a significant upfront investment to ask a company to make, prior to seeing value returned. It can also be error prone given the complexity, with the only practical solution to ongoing management being [infrastructure as code](https://en.wikipedia.org/wiki/Infrastructure_as_code), which requires further investment.

[Day 2 operations](https://dzone.com/articles/defining-day-2-operations) like backups, scaling, and upgrades are equally important. GitLab is a [business critical application](#consistently-great-user-experience-regardless-of-location-or-scale), so events like upgrades need to be routine and [seamless](https://docs.gitlab.com/omnibus/update/README.html#zero-downtime-updates). The easier we make it for our customers to upgrade, the faster our users will be able to leverage our new features and provide feedback. Currently it takes [3 months](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate) after release for half of our users to feel the impact of our work.

By reducing the deployment/operating costs and packaging best practices, we will see the following benefits:

* Increased downloads and trials of GitLab
* Shortened sales cycles as a result of quicker [time to value](https://en.wikipedia.org/wiki/Time_to_value) and lower [total cost of ownership](https://en.wikipedia.org/wiki/Total_cost_of_ownership)
* Improved typical end-user experience across all GitLab instances, with services more likely to be deployed optimally and on a recent version
* Further penetrate the SaaS-leaning market segment with self-managed for those where GitLab.com does not currently meet their needs
* Reduction in cycle time, due to a higher population of users on recent versions

### Consistently great user experience regardless of location or scale

As customers roll out and adopt additional stages of GitLab to realize the benefits of a [single application](https://about.gitlab.com/single-application/), the service availability and performance requirements increase as well. Additional departments within the business utilize the service on a daily basis to accomplish their jobs, such as design, security, and operations teams. People around the world collaborate together. Work that may have been done manually is now automated using CI/CD, including delivering the latest update or bug fix to their customer facing application/website. For these reasons, GitLab is increasingly being identified as a business-critical application with attendant requirements. It is therefore important for GitLab to be a consistently reliable, performant service for all users to fulfill its potential.

By providing market-leading reliability and performance, we will see the following benefits:

* Establish the trust required for customers to adopt additional stages and consolidate on GitLab
* Further penetrate the large enterprise by displacing incumbents with unsatisfactory stability/performance
* Improve our market share of the [SaaS DevOps segment](#saas) by improving the [service level of GitLab.com](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability)
* Increase customer satisfaction by improving responsiveness and availability

### Achieve enterprise compliance needs

Organizations in regulated industries, the [public sector](https://en.wikipedia.org/wiki/Public_sector), and large enterprises often have a variety of standards and processes that they must adhere to.

In the public sector, there are standards like [NIST 800-53](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-53r4.pdf) and [FedRAMP](https://www.fedramp.gov/). For companies handling credit card transactions, there is [PCI DSS](https://www.pcisecuritystandards.org/). These are just two examples. While some of these standards are not directly aimed at services like GitLab, they have a broad reach, and the requirements generally cannot be waived, as the penalties for non-compliance can be severe. Many enterprises have also developed their own internally driven processes and policies that can be important to support, rather than requiring changes prior to the adoption of GitLab.

For published standards, it is important that GitLab offers the required features and configuration to enable our customers to be in compliance. This includes changes to our code base; for example, fully encrypting all traffic between GitLab nodes, selection of specific cryptographic modules, [high availability](https://about.gitlab.com/solutions/high-availability/), and [disaster recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/), among others. Additionally, some standards like [FedRAMP](https://www.fedramp.gov/documents/) can also impact the operational and governance processes of GitLab.com. The more that we can do to be compliant out of the box or provide documentation on recommended settings, the less work our customers will be required to do during evaluation and implementation.

Enterprise-specific standards are also important to support with as little friction as possible. One of the common ways enterprises achieve these is by ensuring their new services plug into existing tools, processes, and workflows. While we try to build the required settings/features into GitLab directly, these workflows and tools can be highly specific to a given enterprise, with our API being frequently relied upon instead. Some examples we encounter include user provisioning, custom analytics and reporting, and auditing. Providing a high quality API with easy-to-use client libraries can speed the integration work and reduce the effort required.

By enabling our customers to meet their compliance requirements and reducing the required effort, we will see the following benefits:

* Increased penetration of these segments: large enterprise, regulated industries, and the public sector
* Shortened sales cycles by reducing the amount of evaluation and implementation work required to ensure they can remain compliant
* Reduced total cost of ownership with higher quality APIs and less custom integration work required

### Elevated contributor experience

As an open source project and [single application](https://about.gitlab.com/single-application/) for the whole DevOps lifecycle, an elevated contributor experience is critical.

Two of the highest priorities in the company are [growing our open source community](https://about.gitlab.com/handbook/leadership/biggest-risks/#6-loss-of-the-open-source-community) and maintaining the [velocity](https://about.gitlab.com/handbook/leadership/biggest-risks/#7-loss-of-velocity) of the project. An improved contributor experience has a direct impact on both of these metrics by reducing initial friction and ensuring seasoned GitLab employees can quickly develop and review changes. For example, consider the time and effort required to set up your [development environment (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit) to contribute or review a change to [AutoDevOps](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops.md) or [Geo](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/geo.md). As the [wider GitLab community grows](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/#contributor-metrics), along with the surface area of the product, the value of a great contributor experience will have compounding returns.

As noted [above](#consistently-great-user-experience-regardless-of-location-or-scale), GitLab plays a central role in an organization's software engineering process. Currently, there are [34 services directly integrated](https://docs.gitlab.com/ee/user/project/integrations/project_services.html), with many of these originally contributed by members of the community and many more through our API. These integrations are important to providing a smooth and productive workflow, especially at larger organizations where there may not be a single standard toolchain. For example, it is not uncommon for some portions of a company to use GitLab Issues and other parts to use Jira. If we did not have Jira integration, these users would be less productive, and our growth within this organization may be constrained. By making it easier to contribute an integration to GitLab, we can increase our coverage area of commonly requested integrations, whether it's from a community member or third-party product.

By providing an elevated contributor experience, we will see the following benefits:

* Accelerated growth of our community and an increased number of repeat contributors
* Increased likelihood of maintaining our velocity
* Accelerate the contribution of integrations, providing an improved user experience and potentially increased organic growth

## One-year plan

Over the next 12 months, each group in the Enablement section will play an integral part in this strategy.

Please see the [categories page](/handbook/product/categories/#enablement-section) for a more detailed look at Enablements's plan by exploring `Strategy` links in areas of interest.

<%= partial("direction/enablement/plans/distribution") %>

<%= partial("direction/enablement/plans/geo") %>

<%= partial("direction/enablement/plans/ecosystem") %>

<%= partial("direction/enablement/plans/search") %>

<%= partial("direction/enablement/plans/memory") %>

<%= partial("direction/enablement/plans/database") %>

<%= partial("direction/enablement/plans/infrastructure") %>

## What's Next

<%= direction["all"]["all"] %>
