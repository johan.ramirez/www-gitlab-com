---
layout: handbook-page-toc
title: "Business System Analysts"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Business Systems Analyst

### Links
*  [Job Description](/job-families/finance/business-system-analyst/)
*  [Epic Board: Business Operations](https://gitlab.com/groups/gitlab-com/business-ops/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=BSA)

### General Ongoing
##### Documentation
For reference, "[What Nobody Tells you about Documentation](https://www.divio.com/blog/documentation/)"
* Tutorials
* How-To Guides
* Explanation
* **Reference** (most of the type we create)

##### IT Capability Matrix
As IT as a department scales and matures, [we must identify how we grow and operate](https://docs.google.com/spreadsheets/d/1AEQBweIZaoI-Z43MkzmL30WLhnmzw3UR3VNtgoDzSPM/edit#gid=0) inline with company values and business requirements.

##### Retrospectives
We can host your project retrospective.

##### Tool Evaluation
We can provide [templates](/handbook/business-ops/#templates) for vendor evaluations, can help review your user stories, and are
happy to participate in tool evaluation that integrates with other tools or intersects with multiple departments.

### What are the BSAs working on?

##### Business Analysis Support
Phil Encarnacion, Business Systems Specialist

##### Finance
Barbara R., Business Systems Analyst
* work closely with Finance Systems Administrator

##### Portal integrations and operations
Jamie Carey, Senior Business Systems Analyst
*  Portal Analysis and Documentation
*  Go to market operations

##### Security and Compliance
Karlia Kue, Business Systems Analyst
*  Offboarding compliance
*  GDPR
*  Business Preparedness

##### IT Operations and People Operations
Lis Vinueza, Business Systems Analyst
*  Embed Tech stack in Handbook
*  Automation of system provisioning
*  Tech Stack: Maintenance
*  Zendesk <> SFDC integration
*  Scale onboarding

### What does a BSA do?
> Note: BSA team needs to create templates for the types of work below

##### Create
- templates
- vendor score cards
- diagrams, documentation

##### Initiate
- BSAs may start by asking questions about the current state of a process, integration, or tool evaluation in order to understand what the pain points are for the people involved. Often this is research that takes interviews of subject matter experts, reading existing documentation, and requesting confirmation of what they are learning as they tie existing information into the rough draft documentation of the current state in new documents and diagrams.
- BSAs also reach out to external vendors to request clarity, roadmap information, and support from account managers, product managers, and support teams.

##### Communicate
- scope (identification of edge cases), analysis, options
- stakeholders
- general project information: directly responsible individual, cost, vendors details
- risks and benefits of design choices
- project organization; communication, documentation, and training plans; escalation path, go-live criteria; meeting notes/recordings
- schedule, status, and milestones achieved
- change management plan

##### Define
- requirements (includes security, legal, and financial requirements)
- user stories (wants, needs, avoids)
- test cases
- unit tests
- user acceptance testing

##### Assign work
- to system provisioners
- to IT
- to BSAs
- to devs
- to vendors/contractors
- to other internal collaborators

##### Document
- current state/future state
- "how to"
- interviews with stakeholders
- feedback
- definitions
- roadblocks/problems
- system integrations/workflows/backend processes
- stakeholder signoff
- training on new/future state
- retrospectives
