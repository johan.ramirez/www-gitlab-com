---
layout: handbook-page-toc
title: "Quality Department"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Child Pages

##### [On-boarding](/handbook/engineering/quality/onboarding/)
##### [Guidelines](/handbook/engineering/quality/guidelines/)
##### [Performance Indicators](/handbook/engineering/quality/performance-indicators/)
##### [Performance and Scalability](/handbook/engineering/quality/performance-and-scalability/)
##### [Project Management](/handbook/engineering/quality/project-management/)
##### [Roadmap](/handbook/engineering/quality/roadmap/)
##### [Test Engineering](/handbook/engineering/quality/test-engineering/)
##### [Triage Operations](/handbook/engineering/quality/triage-operations/)

## Vision

Ensure that GitLab consistently releases high-quality software across the growing suite of products, developing software at scale without sacrificing quality, stability or velocity.
The quality of the product is our collective responsibility. The quality department makes sure everyone is aware of what the quality of the product is, empirically.

To execute on this, we categorize our vision into the following areas.

### Culture of Quality

#### Champion an early culture of quality in the development process
{:.no_toc}
* Review product requirements and designs to identify risk and steer the team on quality strategy.
* Review code in collaboration with counterpart teams to ensure adequate test coverage is present.
* Partner with all development teams to ensure that testability is built in.
* Be a champion for better software design, promote proper testing practices and bug prevention strategies.

#### Timely feedback loop
{:.no_toc}
* Use the data from test results to improve test gaps in our test suites.
* Conduct test gap analysis from bugs that are filed by GitLab users.
* Be a sounding-board for our end users and non-engineering stakeholders by integrating their feedback on quality into the development process.

### Test Coverage

#### Test framework reliability and efficiency
{:.no_toc}
* Expand end-to-end testing framework [GitLab QA].
* Maintain our Continuous Integration & Delivery test pipelines.
* Improve on the duration and de-duplication of the GitLab test suites.
* Improve stability by eliminating flaky tests and transient failures.

#### Improve test coverage
{:.no_toc}
* Groom the [test pyramid](https://martinfowler.com/bliki/TestPyramid.html) coverage and ensure that the right tests run at the right place.
* Improve functional performance test coverage.
* Improve third-party service integration test coverage.
* Improve test results tracking and reporting mechanisms that aid in triaging test results.
* Coach developers (internal & external) on contributing to [GitLab QA] test scenarios.

#### Self-managed usecase
{:.no_toc}
* Build GitLab self-managed reference architecture environments.
* Build test tooling and test processes that validates GitLab reference environments.
* Capture self-managed customer usage and requirements to improve testing and validation.

#### Improve Quality in release process
{:.no_toc}
* Eradicate obstacles in the release process.
* Ensure that we have a repeatable and consistent Quality process for every release.

### Engineering Productivity

* Make metrics-driven suggestions to improve engineering processes, velocity, and throughput.
* Build productivity tooling to help speed up overall Engineering.
* Build automated tooling to speed up issue and merge request review and triage.
* Build automated tools to ensure the consistency and quality of the codebase and merge request workflow.
* Help with maintaining [GitLab Docs](https://gitlab.com/gitlab-org/gitlab-docs)

## Quality Engineering structure

### Teams within Quality Engineering

##### [Dev QE team](dev-qe-team)
##### [Ops & CI/CD QE team](ops-qe-team)
##### [Growth QE team](growth-qe-team)
##### [Secure & Enablement QE team](secure-enablement-qe-team)
##### [Engineering Productivity team](engineering-productivity-team)

### Department members

<%
director_role = 'Director of Quality Engineering'
roles_regexp = /(Engineer in Test|Engineering Productivity|Quality Engineering)/
%>

#### Director and managers

<%= direct_team(role_regexp: roles_regexp, manager_role: director_role) %>

#### Individual contributors

<%= stable_counterparts(role_regexp: roles_regexp, direct_manager_role: director_role) %>

### Stable counterparts

Every Software Engineer in Test is aligned with a Product Manager and is responsible for the same features their Product Manager oversees.
They work alongside Product Managers and engineering at each stage of the process: planning, implementation, testing and further iterations.
The area a Software Engineer in Test is responsible for is part of their title; for example, "Software Engineer in Test, Plan." as defined in the [team org chart](/company/team/org-chart/).

Every Quality Engineering Manager is aligned with an Engineering Director in the Development Department.
They work at a higher level and align cross-team efforts which maps to a [Development Department section](/handbook/product/categories/#hierarchy).
The area a Quality Engineering Manager is responsible for is part of their title; for example, "Quality Engineering Manager, Dev" as defined in the [team org chart](/company/team/org-chart/).
This is with the exception of the Engineering Productivity team which is based on the [span of control](/handbook/leadership/#management-group).

Full-stack Engineering Productivity Engineers develop features both internal and external which improves the efficiency of engineers and development processes.
Their work is separate from the regular release kickoff features per [areas of responsibility](/handbook/engineering/quality/engineering-productivity-team#areas-of-responsibility).

### Staffing planning

We staff our department with the following gearing ratios:

* **One Software Engineer in Test for each group**
  * 1:1 ratio of Software Engineer in Test to [Product Groups](/handbook/product/categories/#hierarchy).
  * Approximately a 1:10 ratio of Software Engineer in Test to Development Department Engineers.
  * Approximately 1:1 ratio of Software Engineer in Test to Product Managers.
  * **Note:** The ratio may change in some product groups due to the complexity of that product area.
* **One Quality Engineering Manager for each section**
  * 1:1 ratio of Quality Engineering Manager to [Development Department Sections](/handbook/product/categories/#hierarchy).
  * Approximately a 1:1 ratio of Quality Engineering Manager to Development Department Directors.
* **One Engineering Productivity Engineer for each section**
  * Approximately a 1:1 ratio of Engineering Productivity Engineer to [Development Department Sections](/handbook/product/categories/#hierarchy).
  * Approximately a 1:37 ratio of Engineering Productivity Engineers to Development Department Engineers.

## Quality Engineering processes

### Quality department pipeline triage rotation

Every member in the Quality Department shares the responsibility of analyzing the daily QA tests against `master` and `staging` branches.
More details can be seen [here](/handbook/engineering/quality/guidelines/#quality-department-pipeline-triage-rotation)

### Development and infrastructure collaboration

We currently have 2 venues of collaboration with [Development](/handbook/engineering/development/) and [Infrastructure](/handbook/engineering/infrastructure/) departments.

#### Availability and performance grooming

To mitigate performance issues, Quality Engineering will triage and groom performance issues for Product Management and Development via a weekly
[Availability & Performance Grooming](https://gitlab.com/groups/gitlab-org/-/boards/1233204). The goal is to make the performance of various aspects of our application empirical with tests, environments, and metrics.

Quality Engineering will ensure that performance issues are identified and/or created on the board with the label `~performance-grooming`.
These issues that are surfaced to the grooming meeting will be severitized according to our definitions.

##### Identifying issues
{:.no_toc}

Quality Engineering will focus in identifying issues in the following areas:
* Existing customer impacting performance bugs [in our issue tracker](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance&label_name[]=customer&label_name[]=bug).
* Early warning on performance degradation which have not had customer exposure but poses a risk in the future.

##### Grooming
{:.no_toc}

A manager in the Quality Engineering department will lead grooming with issues populated before hand in the board. Issues are walked through from high to low severity covering `~S1`, `~S2` and `~S3` performance bugs.

Deliverable of grooming each issue:
* A product manager of a stage or group is assigned to prioritize performance mitigation with a scheduled milestone.
* Long running issues that do not have meaningful activity can be considered for closure if no longer actionable.
* If additional test coverage is needed, a follow up issue to close the performance test gap will be created in [Performance test issue tracker](https://gitlab.com/gitlab-org/quality/performance).

Please see the [Development department's Infrastructure and Quality collaboration](/handbook/engineering/development/#continuous-delivery-infrastructure-and-quality-collaboration) handbook section.

#### Development requests

Quality Engineering will track productivity, metric and process automation improvement work items
in the [Development-Quality](https://gitlab.com/groups/gitlab-org/-/boards/1262515) board to service the [Development](/handbook/engineering/development/) department.
Requirements and requests are to be created with the label `~dev-quality`. The head of both departments will review and groom the board on an on-going basis.
Issues will be assigned and worked on by an Engineer in the [Engineering Productivity team](engineering-productivity-team) team and [communicated broadly](/handbook/engineering/quality/triage-operations/#communicate-early-and-broadly-about-expected-automation-impact) when each work item is completed.

### Meetings

We try to have as few meetings as possible. We currently have 3 recurring meetings for the whole department.
Everyone in the Department is free to join and the agenda is available to everyone in the company. Every meeting is also recorded.

1. **Quality Department Weekly**: This is where the whole department comes together weekly to discuss our day-to-day challenges, propose automation framework improvements and catchup on important announcements.
This is also a place to connect socially with the rest of the department. This meeting happens in 2 parts to accommodate our team members across multiple timezones.
  * Part 1 - Wednesday 0730 UTC
  * Part 2 - Wednesday 2030 UTC (adjusts per PDT)
1. **Engineering Productivity Team Weekly**: The Engineering Productivity team meets weekly to discuss engineering wide process improvements. This meeting is scheduled for every Tuesday at 1300 UTC.
1. **Quality Engineering Staff Weekly**: This weekly brings together Quality Engineering's management team to discuss directional plans, long-term initiatives, hiring goals and address issues that require attention. This meeting is scheduled for every Wednesday at 1430 UTC (adjusts per PDT).

### Team retrospective

The Quality team holds an asynchronous retrospective for each release.
The process is automated and notes are captured in [Quality retrospectives](https://gitlab.com/gl-retrospectives/quality/) (GITLAB ONLY)

### AMA sessions

Every quarter the Quality team will host an AMA session. The idea is to keep everyone informed about what's new, our challenges and to answer questions related to the test framework and etc.

The next sessions are scheduled for 2019/09/20, 2019/12/20, and 2020/03/20.

> Note: the dates mentioned above can change, but we will try to keep this document updated.

### Release process overview

Moved to [release documentation](https://gitlab.com/gitlab-org/release/docs/).

## Quality Engineering initiatives

### Triage Efficiency

Due to the volume of issues, one team cannot handle the triage process.
We have invented [Triage Packages](/handbook/engineering/quality/triage-operations/#triage-packages) to scale the triage process within Engineering horizontally.

More on our [Triage Operations](/handbook/engineering/quality/triage-operations/)

### Test Automation Framework

The GitLab test automation framework is distributed across three projects:

* [GitLab QA], the test orchestration tool.
* The scenarios and spec files within the GitLab codebase under `/qa` in both GitLab [CE] and [EE].

#### Architecture overview

See the [GitLab QA Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs)
and [current architecture overview](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/architecture.md).

#### Installation and execution

* Install and set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
* Install and run [GitLab QA] to kick off test execution.
  * The spec files (test cases) can be found in the [GitLab CE/EE codebase](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/qa)

### Performance and Scalability

The Quality Department is committed to ensuring that self-managed customers have performant and
scalable configurations. To that end, we are focused on creating a variety of tested and certified
[Reference Architectures]. Additionally, we have developed the [GitLab Performance Toolkit], which
provides several tools for measuring the performance of any GitLab instance. We use the Toolkit
every day to monitor for potential performance degradations, and this tool can also be used by
GitLab customers to directly test their on-premise instances. More information is available on our
[Performance and Scalability](/handbook/engineering/quality/performance-and-scalability/)
page.

## Other related pages

* [Issue Triage Policies](/handbook/engineering/issue-triage/)
* [Performance of GitLab](/handbook/engineering/performance/)
* [Monitoring of GitLab.com](/handbook/engineering/monitoring/)
* [Production Readiness Guide](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md)

[GitLab QA]: https://gitlab.com/gitlab-org/gitlab-qa
[GitLab Insights]: https://gitlab.com/gitlab-org/gitlab-insights
[GitLab Performance Toolkit]: https://gitlab.com/gitlab-org/quality/performance
[GitLab Triage]: https://gitlab.com/gitlab-org/gitlab-triage
[CE]: https://gitlab.com/gitlab-org/gitlab-ce
[EE]: https://gitlab.com/gitlab-org/gitlab-ee
[Reference Architectures]: https://docs.gitlab.com/ee/administration/high_availability/README.html#reference-architecture
