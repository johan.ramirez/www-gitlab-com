---
layout: markdown_page
title: Community Advocate Bootcamp Checklist
---

## Bootcamp Checklist for Training of New Community Advocates
<a name="checklist"></a>

Your manager should create an issue with this checklist on the [community advocacy issue tracker](https://gitlab.com/gitlab-com/marketing/community-advocacy/general/issues) for you as part of your onboarding.
This list looks strange in this handbook because it is formatted to be copy-pasted into an issue and then render properly there.

The topics are generally ordered by priority in which they need to be tackled,
but feel free to work on later things in the list when you are waiting on something.

```markdown
We need to keep iterating on this checklist so please submit MR's for any improvements
that you can think of. The file is located in
[`source/handbook/marketing/developer-relations/community-advocacy/onboarding/checklist/index.html.md`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook/marketing/developer-relations/community-advocacy/onboarding/checklist/index.html.md)
in the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/) project. If an item does not start with a role or name of someone else, it's yours to do.

**Goal of this entire checklist:** Set a clear path for Community Advocacy training

### Stage 0: Complete general onboarding to have all necessary accounts and permissions

- [ ] **Done with Stage 0**

_Typically completed within first week_

1. [ ] General expectations: it is expected that you will _start_ to tackle Stages 0, 1, 2, and 3 as early as your first week, but you are not expected to complete all items for some number of weeks. We believe that you often learn best and fastest by diving into (relatively easy) tickets, and learning by doing. However, this has to be balanced with making time to learn some of the tougher topics. The expectation is therefore that you are sufficiently advanced in your onboarding by the end of your first week, that you can answer 2-5 "simple" tickets. Over the following weeks, your manager will set higher targets for the number and difficulty of tickets to be tackled, but you should always have about 2-3 hrs per day to spend on working through this bootcamp. Some days it will be less, some days it will be more, depending on ticket load and how deep "in the zone" you are in either the bootcamp or the tickets you are responding to; but an average of 2-3 hrs per day should allow you to complete everything through to the end of Stage 6 within about four weeks.
1. [ ] General Onboarding Checklist: this should have been created for you on [the People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues/) as an issue by PeopleOps when you were hired.
   1. [ ] Finish every item on that list starting with `New team member:` until you are waiting for someone to answer a question or do something before you can continue that list.
   1. [ ] Start with Stage 1 here, but also with the first steps in Stage 2 and Stage 3.
   1. [ ] Check back daily on what was blocking you on your General Onboarding Checklist until that list is completely done, then focus on this one.

### Stage 1: Become familiar with `git`, GitLab basics, and start exploration of Zendesk.

- [ ] **Done with Stage 1**

_Typically started in first week, completed by end of second week_

## Zendesk:

1. [ ] Check with your manager that you've been given Agent Access to the Community Advocate Zendesk instance.
1. [ ] Quickly check out your Zendesk account to confirm you can login. Explore which platforms are piped into the Zendesk ticket views.
1. [ ] Under your profile on Zendesk, it should read `Agent` or `Admin`. If it reads `Light Agent`, inform your manager.
1. [ ] Add a [profile picture](https://support.zendesk.com/hc/en-us/articles/203690996-Updating-your-user-profile-and-password#topic_rgk_2z2_kh) to your Zendesk account
1. [ ] [Add Facebook to one of your personal views on Zendesk](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/tools/zendesk/#view-limits-workaround). 
1. [ ] Open an [Access Request](/handbook/business-ops/it-ops-team/access-requests/)to be added as a ['Light Agent' on the Support Zendesk Instance](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff). Advocates use this instance to check in on support tickets inquired about over Twitter.

## Git and GitLab Basics:

If you are already comfortable with using Git and GitLab and you will be able to
retain a good amount of information by just reading and watching through, that is
okay. But if you see a topic that is completely new to you, stop the video and try
it out for yourself before continuing.

Cover these topics on the [GitLab University](https://docs.gitlab.com/ee/university/):

1. [ ] Under the topic of Git
  1. [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.gd69537a19_0_14)
  1. [ ] [Try Git](https://www.codeschool.com/account/courses/try-git)
1. [ ] Under the topic of GitLab Basics
  1. [ ] All the [GitLab Basics](http://docs.gitlab.com/ee/gitlab-basics/README.html) that you don't feel comfortable with. If you get stuck, see the linked videos under GLB in GitLab University
  1. [ ] [GitLab Flow](https://www.youtube.com/watch?v=UGotqAUACZA)
  1. [ ] Take a look at how the different GitLab versions [compare](/features/#compare)
1. [ ] Any of these that you don't feel comfortable with in the [user training](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training) we use for our customers.
  1. [ ] `env_setup.md`
  1. [ ] `feature_branching.md`
  1. [ ] `explore_gitlab.md`
  1. [ ] `stash.md`
  1. [ ] `git_log.md`
  1. [ ] For the rest of the topics in `user training`, just do a quick read over the file names so you start remembering where to find them.
1. [ ] Get familiar with the services GitLab offers
  1. [ ] The differences between [CE and EE](/pricing/)
  1. [ ] [GitHost](/gitlab-hosted/)
    * Note that we're currently not accepting any new customers

### Stage 2: Installation and Administration basics

- [ ] **Done with Stage 2** 

_Typically started in first week, completed by end of third week_

#### Goals

- Be prepared to reproduce issues that our users encounter.
- Be comfortable with the different installation options of GitLab.
- Have an installation available for reproducing customer bug reports.


#### Installations

You will install GitLab using Omnibus, which packages services and tools required to run GitLab.

Use the [GitLab Installation Guide](https://about.gitlab.com/install/) for step-by-step directions on the installtion process.

The [Omnibus GitLab Docs](https://docs.gitlab.com/omnibus/#configuring) are a good place to start if you have questions. 

1. [ ] Set up your test environment
  1. [ ] Create a non-root user
  1. [ ] Add your public SSH key to `authorized_keys` for the created user
  1. [ ] Optional: Disable password login
  1. [ ] Optional: Disable root SSH login
1. [ ] Install GitLab via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/)
  1. [ ] Populate with some test data: User account, Project, Issue
  1. [ ] Backup using our [Backup rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
  1. [ ] Delete the test data
  1. [ ] Restore backup using our [Restore rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-a-previously-created-backup)
  1. [ ] Keep this installation up-to-date as patch and version releases become available, just like our customers would.
1. [ ] Ask as many questions as you can think of on the `#questions` chat channel


### Stage 3. Community Interaction

- [ ] **Done with Stage 3**

_Typically started in first week, and completed by the end of the fourth week_

#### Goals

- Have a good understanding of ticket flow through Zendesk and how to interact with our various channels
- See some common issues that our customers are facing and how to resolve them.
- Learn about the community advocacy process at GitLab.

#### Initial Zendesk training

1. [ ] Watch the [Zendesk Support Overview training video](https://training.zendesk.com/zendesk-suite-overview-support/204768) for an overview of how Zendesk functions. Note you will need to register on training.zendesk.com before viewing.
1. [ ] Review additional Zendesk resources
  1. [ ] [UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
  1. [ ] [Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
  1. [ ] [Working w/ Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets) *Read: avoiding agent collision.*
1. [ ] Set up 1 or 2 calls with a Community Advocate and ask them to share their screen to walk you through each Zendesk view.
  1. [ ] On this call, also ask the Advocate to explain how we use Zapier integration with our Zendesk views.
  
*Congratulations. You now have your Zendesk Wings*

From now on you can spend most of your work time answering tickets on Zendesk, try to set aside 2 hours per day to make it through **Stage 4-6** of this list. Never hesitate to ask as many questions as you can think of on the `#devrel` chat channel

#### Learn about raising issues and fielding feature proposals

1. [ ] Understand what's in the pipeline and proposed features at GitLab: [Direction Page](/direction/)
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/gitlab-org/gitlab-ce/labels) to find existing feature proposals and bugs
1. [ ] If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk
1. [ ] Add [customer labels](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name%5B%5D=customer) for those issues relevant to our subscribers
1. [ ] Take a look at the [existing issue templates](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#issue-tracker) to see what is expected
1. [ ] Raise issues for bugs in a manner that would make the issue easily reproducible. A Developer or a contributor may work on your issue.
1. [ ] Set up [Google search shortcuts](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/searching/) to make it easier to locate docs and issues to share with the community 
1. [ ] Schedule a call with an advocate to discuss how the GitLab community uses issues to report bugs they find. Discuss strategies for using issues as a place to research answers and support for community members.

#### Explore the Community Relations Subgroups and Projects

1. [ ] Read the [Issue Board Documentation](https://docs.gitlab.com/ee/user/project/issue_board.html) to learn how issue boards are used within GitLab.
1. [ ] Review the different projects on the [Community Relations Subgroupds and Projects](https://gitlab.com/gitlab-com/marketing/community-relations). These subgroups and projects track issues and MRs that the team is working on. 
1. [ ] Review the [Community Advocacy Issue Board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/781438?&label_name[]=Community%20Advocacy). This board holds issues specific to the advocates team. Each should be labeled with the 'Community Advocacy' tag. Advocates use 'status::plan' and 'status::wip' tags to track current work.
1. [ ] Look over the advocate issue boards for [Merchandise](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/944793?&label_name[]=Merch), [Open Source](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/944792?&label_name[]=Open%20Source), and [Education](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/952267?&label_name[]=Education).
1. [ ] Other Community Relations issue boards are helpful to grow your understanding of larger team projects and goals. 



#### Learn about the Community Advocacy process

Zendesk is our Community Advocacy Centre and our main communication line with our customers. We communicate with customers through several other channels too.

1. [ ] Ask a few community advocates if they would be willing to do a 45 minute screen share with you as they answer tickets on Zendesk, thinking out loud as much as they can and answering your questions. Continue with the rest of the list while you wait for these to get scheduled.
  1. [ ] call with ___
  1. [ ] call with ___
  1. [ ] call with ___
1. [ ] Dive into our ZenDesk support process by reading how to [handle tickets](/handbook/marketing/developer-relations/community-advocacy/#zendesk-workflow)
1. [ ] Start getting real world experience by handling real tickets, all the while gaining further experience with the Product.
  1. [ ] First, learn about our support channels that are handled by the [Community Advocacy team](/handbook/marketing/developer-relations/community-advocacy)
  1. [ ] Start with [Twitter](/handbook/marketing/developer-relations/community-advocacy/#twitter)
  1. [ ] Continue on to [Disqus](/handbook/marketing/developer-relations/community-advocacy/#disqus)
  1. [ ] Continue on to the [community@](/handbook/marketing/developer-relations/community-advocacy/#community-email) email
  1. [ ] Continue on to [facebook](/handbook/marketing/developer-relations/community-advocacy/#facebook)
  1. [ ] Continue on to the [GitLab forum](/handbook/marketing/developer-relations/community-advocacy/#gitlab-forum)
  1. [ ] Read about the [other channels](/handbook/marketing/developer-relations/community-advocacy/#specific-channels) the Community Advocacy team supports
1. [ ] Check out your colleagues' responses
   1. [ ] Read through about 20 old tickets that your colleagues have worked on and their responses
1. [ ] Take a look at the [GitLab.com Team page](/company/team/) to find the resident experts in their fields
1. [ ] Learn about our [mentions-of-gitlab](/handbook/marketing/developer-relations/community-advocacy/#mentions) Slack channel
  * This tracks all channels that are not routed through ZenDesk
  * HackerNews is routed through this channel - This is the top priority channel
- [ ] Block off your calendar to anticipate for release posts
  - They occur regularly every 22nd in the month, just like each GitLab release.
- [ ] Order, expense, and read the Art of Community by Jono Bacon ([Amazon Link](https://www.amazon.com/Art-Community-Building-New-Participation/dp/1449312063))
1. [ ] Schedule a call or reach out via slack to advocates at any point with questions on the workflow or with a request to walk through a few ticket examples.
1. [ ] Review the [Community Advocate issue board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/781438?label_name[]=Community%20Advocacy) and review current projects the team is working on.

#### Learn about handling our diversity sponsorship program

1. [ ] Read about our [Diversity Sponsorship Program](/handbook/marketing/developer-relations/community-advocacy/#diversity-sponsorship)
1. [ ] Take a look at the [Diversity Sponsorship applications](https://docs.google.com/spreadsheets/d/1FQM7rZ0wdpdZZ4Z7Yk3URjDvHripDRkkiiu0a9usQhk/edit?usp=drive_web&ouid=112392573724280886461)
1. [ ] Discuss with your trainer what qualifies an event for the diversity sponsorship program
1. [ ] Memorize our sponsorship acceptance/declination [email templates](/handbook/marketing/developer-relations/community-advocacy/#email-templates)

#### Learn about handling swag at GitLab

We use Shopify for [our storefront](https://shop.gitlab.com) and [Printfection](http://www.printfection.com/) for sourcing swag.

1. [ ] Review the general [Merchandise Workflow](/handbook/marketing/community-relations/evangelist-program/workflows/merchandise.html)
1. [ ] Learn about our [MVP appreciation gifts](/handbook/marketing/developer-relations/community-advocacy/#mvp-appreciation-gifts)
1. [ ] Learn about our [`#swag` channel](handbook/marketing/community-relations/evangelist-program/workflows/merchandise.html#handling-swag)

### Stage 4. Gathering Diagnostics

- [ ] **Done with Stage 4**

_Typically do this around the third week._

**Goal** Understand the gathering of diagnostics for GitLab instances

1. [ ] Learn about the GitLab checks that are available
    1. [ ] [Environment Information and maintenance checks](http://docs.gitlab.com/ee/raketasks/maintenance.html)
    1. [ ] [GitLab check](http://docs.gitlab.com/ee/raketasks/check.html)
    1. [ ] Omnibus commands
        1. [ ] [Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
        1. [ ] [Starting and stopping services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
        1. [ ] [Starting a rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#invoking-rake-tasks)

### Stage 5. Optional Advanced GitLab Topics

Discuss with your training manager if you should stop here and close your issue
or continue. Also discuss which of the advanced topics should be followed. Do
not just do all of them as they might not be relevant to what customers need
right now and can be a significant time investment.

These are some of GitLab's more advanced features. You can make use of
GitLab.com to understand the features from an end-user perspective and then use
your own instance to understand setup and configuration of the feature from an
Administrative perspective

- [ ] Set up and try [Git LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html)
- [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings
- [ ] Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/workflow/importing/migrating_from_svn.html)
- [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)
- [ ] Create your first [GitLab Page](https://docs.gitlab.com/ee/pages/administration.html)
- [ ] Set up a [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit) instance
- [ ] Get familiar with the GitLab source code by finding the differences
between the [EE codebase](https://gitlab.com/gitlab-org/gitlab-ee) and the [CE codebase](https://gitlab.com/gitlab-org/gitlab-ce)
```
