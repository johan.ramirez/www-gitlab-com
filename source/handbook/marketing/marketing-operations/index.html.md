---
layout: handbook-page-toc
title: "Marketing Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Marketing Operations

Marketing Operations (MktgOps) supports the entire Marketing team to streamline processes and manage related tools. Due to those tools, we often support other teams at GitLab as well. MktgOps works closely with Sales Operations (SalesOps) to ensure information between systems is seamless, data is as accurate as possible and terminology is consistent in respective systems. Not only are we assisting with marketing operations but we are also involved in the operations of marketing, such as the budget and strategies.

## Important Resources
- [Marketing Metrics](/handbook/marketing/marketing-operations/marketing-metrics)
- [Marketing Owned Provisioning Instructions](/handbook/marketing/marketing-operations/marketing-owned-provisioning)
- [List Imports](/handbook/marketing/marketing-operations/list-import)

## Tech Stack  

For information regarding the tech stack at GitLab, please visit the [Tech Stack Applications page](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) of the Business Operations handbook where we maintain a comprehensive table of the tools used across the company.   

The main tools used by Marketing and integrated with Salesforce are:
- [Marketo](/handbook/marketing/marketing-operations/marketo)
- [Outreach.io](/handbook/marketing/marketing-operations/outreach)
- [LeanData](/handbook/marketing/marketing-operations/leandata)
- [PathFactory](/handbook/marketing/marketing-operations/pathfactory)
- Demandbase
- DiscoverOrg
- LinkedIn Sales Navigator
- [Drift](/handbook/marketing/marketing-operations/drift)
- Bizible

Other tools directly used by Marketing and maintained by Marketing Operations:  
- [Bizzabo](/handbook/marketing/marketing-operations/bizzabo)
- Cookiebot
- Disqus
- Eventbrite
- Funnelcake
- Google Adwords
- Google Analytics
- Google Search Console
- Google Tag Manager
- MailChimp
- MozPro
- Sprout Social
- Survey Monkey
- Tweetdeck
- WebEx
- YouTube


## Working with Marketing Operations

### MktgOps Motto: If it isn't an Issue, it isn't OUR issue.   

The MktgOps team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~MktgOps label anywhere within the GitLab repo. 

MktgOps uses a [global issue board](https://gitlab.com/groups/gitlab-com/-/boards/825719) and will capture any issue in any group/sub-group in the repo. There is also a [`Marketing Operations` project](https://gitlab.com/gitlab-com/marketing/marketing-operations) within the [`Marketing` project](https://gitlab.com/gitlab-com/marketing).     

Labels to use:  
- `MktgOps`: Issue initially created, used in templates, the starting point for any label that involved MktgOps
- `MktgOps - FYI`: Issue is not directly related to operations, no action items for MktgOps but need to be aware of the campaign/email/event
- `MktgOps - Reporting`: Any reporting request for MktgOps
- `MktgOps - List Import`: Used for list imports of any kind - event or general/ad hoc
- `MktgOps-Priority::1 - Top Priority`: Issue that is related to a breaking change, OKR focus, any other prioritized project by MktgOps leadership. This category will be limited because not everything can be a priority.
- `MktgOps-Priority::2 - Action Needed`: Issue has a specific action item for MktgOps to be completed with delivery date 90 days or less from issue creation date. This tag is to be used on projects/issues not owned by MktgOps (example: list upload).
- `MktgOps-Priority::3 - Future Action Needed`: Issue has a specific action item for MktgOps, the project/issue is not owned by MktgOps and delivery or event date is 90 days or more from issue creation.
- `MktgOps::1 - Planning`: Issues that are currently being scoped/considered but are not being actively worked on.
- `MktgOps::2 - On Deck`: Issues that have been scoped/considered and will be added to the In Process queue next. 
- `MktgOps::3 - In Process`: Issues that are actively being worked on in the current week/sprint (week? Month? Two-week period?)
- `MktgOps::4 - UAT`: Issues that MktgOps has completed its required tasks for and is ready for review and approval by the Requester/Approver.
- `MktgOps::5 - On Hold`: Issue that is not within existing scope of Mktg OPS current targets, blocked by MktgOps-related task/issue, or external (non-GitLab) blocker.
- `MktgOps::6 - Blocked`: Issue that is currently being worked on by Mktg Ops and at least one other team wherein MktgOps is waiting for someone else/another team to complete an action item before being able to proceed.
- `MktgOps::7 - Completed`: MktgOps has completed their task on this issue although the issue may not be closed. 


## Operations Work Cadence   

The MktgOps team works in two week sprints which are tracked as **Milestones** at the `GitLab.com` level. Each Ops individual contributor (IC) is responsible for adding issues to the milestone that will be completed in the two week time frame. If needed, the IC will separate the main issue into smaller pieces that are *workable* segments of the larger request.   

The MktgOps team will only create a milestone one beyond the current iteration, so at any given time there will be the **current** milestone and **upcoming** milestone, any other issue that is not included will be added into future milestones &/or added as work is completed in the current milestone.   

A milestone cannot be closed nor marked complete until all associated handbook updates have been completed. Within every milestone there is an issue to track all changes and keep a running record of the handbook updates made. The handbook change issue will be created using the `milestone_handbook_update` issue template and automatically added to the [Handbook Change Epic](https://gitlab.com/groups/gitlab-com/-/epics/140).  

### Operational Timeline of Changes  

Periodically Marketing Operations makes significant changes to our system and processes that affect overall tools, data and reporting or uncovers significant changes that affected reporting. As such we have an [Operational timeline of events](https://drive.google.com/open?id=1vhGvEszndMJ4B9EshGFSdTTABwUzBzDObz93vkMSFGA). The MktgOps team updates this document as needed as changes are made. 


## How-tos & FAQs

### Webcast Setup   
When creating a live or ondemand webcast it is important to integrate the event program across the platforms/programs used - GitLab Marketing site (`about.gitlab.com`), Marketo (Marketing Automation), Zoom (Webcast video software) and Salesforce (CRM). This provides transparency about the webcast to anyone with access to Salesforce, namely the Sales and Marketing teams.  

For a comprehensive overview on how to set up a webcast, please visit the [Business Operations section](/handbook/business-ops/resources/#Webinars) of the handbook.  

## Marketing Expense Tracking

| GL Code | Account Name | Purpose |
| :--- | :--- | :--- |
| 6060 | Software Subscriptions |All software subscriptions |
| 6100 | Marketing|Reserved for Marketing GL accounts|
| 6110 | Marketing Site|Not used - All agency fees and contract work intended to improve the marketing site |
| 6120 | Advertising|All media buying costs as well as agency fees and software subscriptions related to media buying |
| 6130 | Events|All event sponsorships, booth shipping, event travel, booth design, event production as well as agency fees and software costs related to events |
| 6140 | Email|All 3rd party email sponsorships as well as agency fees and software costs related to mass email communications and marketing automation |
| 6150 | Brand|All PR, AR, content, swag and branding costs |
| 6160 | Prospecting|Not used - All costs related to prospecting efforts |

### Invoice Approval

Marketing Operations approves any invoices that have not already been coded and approved through a Finance issue or that exceed the original cost estimate. We make use of Tipalti for this process. Team leads will confirm that services were performed or products were received also through Tipalti. Campaign tags are used to track costs related to events and campaigns.

## Lead Scoring, Lead Lifecycle, and MQL Criteria
A breakdown of scoring, lifecycle, and the criteria that drives MQL conversion can be found in the [business operations handbook](/handbook/business-ops/resources/#mql-definition).

## Campaign Cost Tracking
Marketing Program Managers track costs associated with campaigns - such as events, content, webcasts, etc. Campaign tags can be applied to Expensify reports, corporate credit card charges, and vendor bills processed by Accounts Payable. Campaign expenses that are incurred by independent contractors should be clearly noted with the appropriate tag and included in their invoices to the company. We then use these accumulated campaign tag costs for budget to actual analysis as well as to update the Salesforce campaigns with actual costs.

**The following steps are used to create and manage campaign tags:**

1. Event Owners create the campaign tag in the budget document as well as add a link to a Finance issue if it exists.
1. Finance is notified by the budget document to open the tag in NetSuite, which then updates Expensify nightly.
1. Event Owners create the Finance issue for approvals and the MPM issue for tracking and add the same tag to both.
1. MPM receives confirmation from responsible team (i.e. Field Marketing, Content, etc.) that the budget for the campaign has been approved and uses this as the exact same name to set up the Salesforce campaign.

**Things to Note:**

* All costs, including travel expenses for those working the event, must be tagged in order to capture the true cost of campaigns. Although travel expenses related to putting on the event hit a different GL code, they should be budgeted within the event line.
* Tagging expenses that are processed by Accounts Payable require Marketing to provide explicit instruction on when to apply tags, which should happen during the normal course of reviewing and approving vendor invoices.
* For event or campaign expenses that do not have a tag, include a note to Accounts Payable clearly stating that campaign tags are not applicable to the expense. In some cases, a general tag like Swag_Corporate may be more a more appropriate tag to track against budget.

## Marketing Gearing Ratios
Gearing ratios are used as business drivers to forecast long term financial goals by function. Refer to the [FP&A handbook](/handbook/finance/financial-planning-and-analysis/#business-drivers) for further details on how gearing ratios enable planning and forecasting. 

The gearing ratios for marketing are as follows:

- **Inquiries per MQL**: at the top of the marketing funnel, this is the conversion rate at which [Inquiries](/handbook/business-ops/resources/#glossary) become Marketo Qualified Leads ([MQL's](/handbook/business-ops/resources/#mql-definition)).

- **MQL to SAO**: this is the mid-funnel conversion rate at which MQL's become Sales Accepted Opportunities ([SAO's](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)).

- **SAO to Closed-Won**: at the bottom of the marketing funnel, this is the rate at which SAO's move through the full pipeline to [closed won](/handbook/sales/#forecast-categories-definitions), resulting in a sale. 

- **Pipe-to-Spend**: is the ratio of pipeline created that is attributed to demand generation program spend. The target ratio is 5:1 for pipe-to-spend.

- **XDR MQL Disposition/Month**: is the capacity of an ([XDR](/handbook/marketing/revenue-marketing/xdr/)) to convert MQL's into SAO's. Total MQL's converted to SAO's / Qty of XDR's

- **New IACV Average Deal Size**: is the average deal size of new customer transactions by segment and geography

- **Marketing E/R and long-term profitability target**: is marketing operating expense divided by revenue (ratable) as shown on the income statement. The long term target profitability target for Marketing E/R is 13%. This target includes the operating expense related to free usage of gitlab.com. See gitlab financial model for yearly targets of this gearing ratio.

## Email Management

Email database management is a core responsibility for MktgOps. Ensuring GitLab is following email best practices, in compliance with Global spam laws and overall health of active database are all priorities.   

Email creation, campaigns, follow up reporting and sending is the responsibility of the Marketing Program Managers. To request an email of any kind, please see the [instructions](/handbook/business-ops/resources/#requesting-an-email) in the Business Ops section of the handbook.


### Email Communication Policy  

At GitLab, we strive to communicate with people in a way that is beneficial to them. Most of our email marketing communications follow an explicit opt-in policy, although at times, we will communicate via email to people who have not explicitly opted-in. We do this to offer something of value (ex. an invite to a workshop, dinner, the opportunity to meet an industry leader, etc. not an email inviting to read a blog post) to the person. We always include the unsubscribe link in our communications, and we respect the unsubscribe list. In addition to the unsubscribe button at the bottom of all of our emails, we have available our [Email Subscription Center](/company/preference-center/), where people can control their email communication preferences. There are currently four [email segments](/handbook/marketing/marketing-sales-development/marketing-operations//#email-segments).

### Email Segments

Database segments and how someone subscribes to specific segment:  

- **Newsletter**: Users can [subscribe to the newsletter](/company/contact/) through the blog, Contact us page, and CE download page.
- **Security Alerts**: [Subscribe to security notices](/company/contact/#security-notices) on the GitLab Contact us page.
- **Webcasts**: When someone registers to a live or on-demand webcast
- **Live Events**: When someone registers to attend a live event, meet up or in-person training. Use of this segment is narrowed down by geolocation so notification and invitation emails are specific to related area.  

### Types of Email

**Breaking Change Emails**  
These are transactional emails, almost always to our user base, that provide very selective needed information. This is an operational-type email that overrides the unsubscribe and would not need to comply with marketing email opt-out. Usage example: GitLab Hosted billing change, Release update 9.0.0 changes, GitLab Page change and Old CI Runner clients.
It is very important to have Engineering and/or Product team (whoever is requesting this type of email) help us narrow these announcements to the people that actually should be warned, so we are communicating to a very specific targeted list.

**Newsletter**  
Sent bi-monthly (every 2 weeks). Content Team is responsible for creating the content for each Newsletter.  

**Security Alerts**  
Sent on an as needed basis containing important information about any security patches, identified vulnerabilities, etc. related to the GitLab platform. These emails are purely text based and again are transactional in nature.

**Webcasts**   
Invitation and/or notification emails sent about future webcasts.   

**Live Events**   
Invitation emails to attend a live event (VIP or Executive Lunch), meet-up, or in-person training. These emails are sent to a geo-locational subset of the overall segment. This type of email is also used when we are attending a conference and want to make people aware of any booth or event we may be holding and/or sponsoring.


## Website Form Management

The forms on about.gitlab are embedded Marketo forms. Any changes to the fields, layout, labels and CSS occur within Marketo and can be pushed live without having to make any changes to the source file on GitLab. When needing to change or embed a whole new form, ping MktgOps on the related issue so appropriate form and subsequent workflows can be created.

Each Marketo form should push an event after successful submission to trigger events in Google Analytics. We use the following event labels to specify which events to fire.

1. `demo` for static demos on `/demo/` and `/demo-leader/`
1. `webcasts` for forms on any page in `/webcast/`
1. `trial` for the form on `/free-trial/`
1. `resources` for forms on any page in `/resources/`
1. `events` for forms on any page in `/events/`
1. `mktoLead` legacy custom event label used on `/public-sector/` and Newsletter subscription form submission events.
1. `mtkoformSubmit` legacy custom event label used on `/services/` and `/sales/` contact forms.

We add the following line above `return false` in the form embed code. Please update the event label from `demo` to reflect the appropriate form completion.

```
dataLayer.push({event: 'demo', mktoFormId: form.getId()});
```

In the event Marketo has an outage and/or the forms go offline, the forms with highest usage/visibility (Free Trial and Contact Us) have been recreated as Google forms that can be embedded on the related pages as a temporary measure to minimize any effect till the outage is past.
