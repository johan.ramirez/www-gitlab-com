---
layout: handbook-page-toc
title: "Group Conversations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Group Conversations are optional recurring meetings providing regular updates from GitLab teams on a [rotating schedule](/handbook/people-group/group-conversations/#schedule). They are scheduled by [People Experience Associates](/job-families/people-ops/people-experience-associate/) and will automatically appear on your calendar. Everyone is invited to participate by adding questions and comments to the Google Doc linked in the calendar invite. Non-confidential group conversations are streamed live and shared publicly to our [GitLab Unfiltered YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e).

Over the years, GitLab has had Functional Group Updates and recently we've evolved FGUs to be more conversational and engaging.

In this video our CEO, Sid gives our team tips and tricks for their FGU. This still applies to Group Conversations.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/MN3mzvbgwuc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Below is a guide to help everyone get the most out of these conversations. If you have suggestions for how to make the attendee or meeting leader experience please create an MR to update this page — [everyone can contribute](/company/strategy/#why)!

## For Attendees

Attendance is optional. If you are unable to attend the Group Conversation at its scheduled time, don't let that hold you back! Watch the latest Group Conversation in the Google Drive folder found in the description of each update.  If you have questions or a discussion to start, bring it to the `#group-conversations` Slack channel! Make sure to @tag the presenter!

1. Please enter your questions or comments on the linked Google Doc. Preface your question with your full name (first and last name) because there might be other people with your first name on the call and it's helpful to newcomers if they are distinguished.
1. Keep in mind that Group Conversations are recorded and shared publicly and that it's okay to opt out of using your name due to safety and privacy concerns.
1. Please do not include customer names in your questions/comments.
1. Be ready to ask your question out loud as it comes up in the queue.
1. Please don't use the Zoom chat to add information but add them to the doc so people reading the agenda after the call can also benefit.
1. Not everything has to be a question. If you have a comment, bias to putting it into the Google Doc so that those who weren't able to attend the meeting live can see what you had to say (and any responses that arose from it).
1. Thanking and recognizing people is very important.
1. You can ask someone to present a slide to get more context.
1. It's okay to add a question to the end of the queue in the Google Doc as the conversation is taking place.
1. You can contribute taking notes to transcribe the answers during the conversation. Thanks!
1. Make sure there are at least 5 empty items in the list (that just contain a space) for people to add new questions.

## For Meeting Leaders

### Scheduling

Calls are scheduled by [People Experience Associates](/job-families/people-ops/people-experience-associate/). If you need to reschedule, please *switch* your presenting day with another Group Conversation leader, by asking another leader in the #group-conversations channel publicly. People Experience Associates are not responsible for finding a replacement for your day. If you've agreed to switch, please do the following:
  - Go to the *GitLab Team calendar* invite
  - Update the date of your and the other invite to be switched
  - Choose to send an update to the invitees
  - _If prompted_ with the questions to update 1 or all events, choose to only update this event

### Preparation Tips

A little bit of prepartion can go a long way in making the call worthwhile for everyone involved. People tend to spend at least an hour to prepare their updates, you write down everything people should know and present only for a few minutes.

#### Logistics

1. You can invite someone within your team to give the update. It doesn't need to be the team lead, but the team lead is responsible for making sure that it is given.
1. Make sure to add the link of the presentation to the Group Conversation Agenda Document invite at least 24 hours _before_ the call takes place. This allows team members to see the presentation, to click links, have [random access](https://twitter.com/paulg/status/838301787086008320) during the presentation,  and to quickly scan the presentation if they missed it. Please also add who the speaker will be to the presentation and the invite. People Ops will ping the appropriate team member at the 24-hour mark if the event hasn't been updated yet.
1. Drop links to the Group Conversation Agenda Document and the slides in the #company-announcement channel 24 hours before the call.
1. Consider blocking off the 30 minutes before your scheduled to lead a Group Conversation

#### Presentation

There are three layers of content in a presentation:
  - Data, this is the contents the slide.
  - Take away, this is the title of the slide, so use: 'migration 10 days ahead of schedule', instead of 'migration schedule estimates', the combined titles of your slides should make a good summary.
  - Feelings, this is the verbal and non-verbal communication in the video feed, how you feel about the take away, 'I'm proud of the band for picking up the pace'.

1. Save time and ensure asynchronous communication by writing information on the slides. Many people will not be able to participate in Group Conversations, either live or recorded, but can read through the slides.
2. Slides with a lot of text that can be read on their own with lots of links are appreciated.
3. Please add the link to your Group Conversation recordings folder in Google Drive to the invite. This enables team members to easily review the recording afterward.
4. If you want to present please consider [posting a recording to YouTube](/handbook/communication/youtube/) at least a day before the meeting, link it from the Google Doc, and mention it in the relevant slack channels.
5. Once a quarter, add a slide covering items being actioned from the engagement survey.
6. Use this [slide deck](https://docs.google.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing) as template to your presentation. Presentations should allow editing (preferred) or commenting from everyone at GitLab.

### 24 Hours Before the Call

1. Ensure the slide deck is visible to all of GitLab
1. Give a heads up in #company-announcements on Slack if there is a video for the group conversation that you'd like people to watch beforehand.

### 30 Minutes Before the Call

1. Open the questions Google Doc linked from the invite and skim the questions to get a sense of what you can expect.
1. Enable “Show document outline” under the View menu to navigate the document more easily.
1. Improve the hygiene of the questions doc to use numbered lists instead of bulleted lists, so you can refer to questions by number if needed later.
1. Reduce distractions for yourself and the attendees by:
   - having the presentation open in its own new browser window, and only sharing that window in Zoom.
   - switching off notifications (from Slack, email, etc.). On Mac, in the notification center in the upper right hand corner, click on Notifications, select Do Not Disturb (sometimes you need to scroll up to see the option).

## During the Call

### Livestreaming the Call

All group conversations (Public and Private) will be [Livestreamed with Zoom](/handbook/communication/youtube/#livestream-with-zoom) to the [GitLab Unfiltered Channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e).
The GC's DRI is responsible for starting the livestream.

Reminders:
- Close or mute the YouTube page to avoid an echo; you do not need to monitor it during the presentation. Zoom will show *Live on Youtube* on the meeting.
- Announce that the livestream is live and is public or private in the call.
- Remember to only share the slides if illustrating something, otherwise let the speakers be visible in **speaker** view setting on zoom.
- End the zoom call for everyone immediately at the end of the meeting as it subsequently ends the livestream. Press End Meeting and then End Meeting for All.
The calendar events for all livestreamed Group Conversations should adhere to our YouTube [visibility guidelines](/handbook/communication/youtube/#visibility).
Note: If you will be out of office on the day of your Group Conversation, or need a person other than the DRI listed in the [Schedule & DRI table](/handbook/people-group/group-conversations/#schedule--dri):
- Please update the Alternate Host line in the Group Conversation Agenda document.
- Please notify any of the People Ops Specialists in the #group-conversations Slack channel so that they may change the host in Zoom's settings.

### Other things to Note

1. Right now, everyone at GitLab the company is invited to each call, we'll invite the wider community later.
1. Make sure there is a list of 15 numbered items that only contains a space, this will make it easier to add things.
1. Join the call 2-3 minutes early to ensure you're ready when it starts.
1. Right before the call begins (5 seconds or so before the top of the hour), the call moderator will press the Record button and everyone will hear "This call is being recorded".
1. If the call is to be livestreamed, the moderator will ask: "May I start the Countdown?" The call host will either verbalize yes or give a thumbs up. The moderator will state: "Thank you, I'll start the countdown for this Public/Private Livestream in 3,2,1" and the moderator will then start the livestream following the [handbook directions](/handbook/communication/youtube/#starting-a-recorded-video-that-isis-not-livestreamed).
1. When the meeting starts introduce yourself and say a few words about what this call is about. For example, "Hello everyone, I'm Diane and I lead the [team]. I am looking forward to answer your questions about our [group name] group."
1. Do not present your slides. Invite the first person to verbalize their question, respond and when relevant mention a slide number. This is similar to what we do during [board meetings](/handbook/board-meetings/).
1. It is okay to wait until at least two questions have been entered before starting to answer questions, and allow some time for folks to read the slides at the start of the call.
1. If someone can't verbalize their question (not in call, driving, audio problems) read it aloud for people watching only the video.
1. Tone should be informal, like explaining to a friend what happened in the group last month, and shouldn't require a lot of presentation.
1. It's the responsibility of the team members of the group to ensure the content is distributed, this includes ensuring appropriate notes are taken in the [Group Conversation Agenda](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU/edit)
1. This meeting is scheduled to be 25 minutes long. Please keep an eye on the clock and end the meeting on schedule. This meeting must end no later than 29 minutes after the hour.
1. It is appropriate to end the call early if there are no more questions. We aim for results, not for spending the time allotted. Please avoid the temptation of presenting slides to fill time.
1. Don't do a countdown when you're asking for questions, people will not ask one. Just end the call or even better say: we'll not end the call before getting at least one question.
1. If there are more questions or a need for longer conversation, mention on what chat channel the conversation can continue or link to a relevant issue.

## After the Call

1. All calls are published live to YouTube, with the exception of Finance, Sales, Security, Channel, and Partnerships. Every call is reviewed live to change livestream to private should anything confidential arise.
1. The conversation is also announced on and the recording linked from our company call agenda.
1. Calls are 5 times a week 30 minutes before the company call, 8:00 to 8:25am Pacific.
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos", which is accessible to all users with a GitLab.com e-mail account.
1. Video recordings will be published on our blog so contributors, users, and customers can see it. We're aiming to publish a blog post once a week of that weeks' recordings with the matching slides in one go.
1. Slides with a lot of text that can be read on their own with lots of links are appreciated.
1. GC DRIs are encouraged to add uploads to the [Group Conversations playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e) within the GitLab Unfiltered YouTube channel. You may also use Google to [search for past Group Conversations](https://www.google.com/search?&hl=en-us&biw=1366&bih=921&tbm=vid&ei=HFzuXc7WLYO3ggeMxIG4BQ&q=gitlab+unfiltered+group+conversation&oq=gitlab+unfiltered+group+conversation&gs_l=psy-ab.3..33i160k1.23243.31186.0.31344.38.31.0.7.7.0.144.2401.25j5.30.0....0...1c..64.psy-ab..1.36.2340...0j0i8i30k1j0i30k1j33i299k1.0.45wyTBKz0bI). 

## Template for the blog post

For instructions on how to create a blog post, please see our [Blog Handbook](/handbook/marketing/blog/#create-a-new-post).

Please copy the code block below and paste it into a blog post with the file name `yyyy-mm-dd-group-conversation.html.md`.

```md
---
title: "GitLab's Functional Group Updates - MMM DD-DD" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: Functional Group Updates
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Company call](/handbook/#company-call), a different Group initiates a [conversation](/handbook/people-group/group-conversations/) with our team.

The format of these calls is simple and short where attendees have access to the presentation content and presenters can either give a quick presentation or jump straight into the agenda and answering questions.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

```

Presentation - https://example.com [Input your presentation's URL and check if its set to view only for anyone with the link if using Google Slides]

- After the video is done uploading, click "Video Manager" in the bottom right corner.
- Edit the video to start when the meeting actually starts:
  - Click **Edit** next to the video icon.
  - Click the **Enhancements** tab on the top menu bar.
  - Click **Trim** on the bottom right. Slide the left edge of the bar to a few moments before the presentation begins, and the right edge of the bar to a few moments after the presentation ends. Click **Done**.
- Take a screenshot of the second slide of the presentation to make it as custom thumbnail for your video on YouTube. You can upload your custom thumbnail under the **Info and Settings** tab when you are editing a video.
- After the video is finished being edited, change the security level back to "public".
- Make sure to use correct format for [embedding videos from YouTube](/handbook/engineering/technical-writing/markdown-guide/#display-videos-from-youtube) into the blog post. Replace the video URL only.

## Schedule & DRI

There is a rotating schedule with each functional group having a conversation on a regular interval. The schedule with directly responsible individuals (DRI) is as follows:
Kindly note there will be no Group Conversations or Company Calls starting on December 23, 2019 and we will reconvene with company calls and Group Conversations on January 2, 2020. 

| Week | Day | Group Conversation | DRI |
|---|---|---|---|
| One  | Mon  | [Secure Section](/handbook/product/categories/#secure-section)  | David DeSanto  |
| One  | Tue  | [Ops Section](/handbook/product/categories/#ops-section)  | Kenny Johnston  |
| One  | Wed  | [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)  | Evan Welchel  |
| One  | Thur  | [Marketing](/handbook/marketing/)  | Todd Barr  |
| One  | Fri  | Open  | Open  |
| Two  | Mon  | [Enablement](/handbook/engineering/development/enablement/)  | Josh Lambert  |
| Two  | Tue  | [Dev Section](/handbook/product/categories/#dev-section)  | Eric Brinkman  |
| Two  | Wed  | [Quality](/handbook/engineering/quality/)  | Mek Stittri  |
| Two  | Thur  | [UX](/handbook/engineering/ux/)  | Christie Lenneville  |
| Two  | Fri  | Open  | Open  |
| Three  | Mon  | [Growth Section](/handbook/product/categories/#growth-section)  | Scott Williamson  |
| Three  | Tue  | [Support](/handbook/support/)  | Tom Cooney  |
| Three  | Wed  | [Development](/handbook/engineering/development/)   | Christopher Lefelhocz  |
| Three  | Thur  | [Finance](/handbook/finance/)  | Paul Machle  |
| Three  | Fri  | Open  | Open  |
| Four  | Mon  | [Defend Section](/handbook/product/categories/#defend-section)  | David DeSanto  |
| Four  | Tue  | [General](/handbook/ceo/)  | Sid Sijbrandij   |
| Four  | Wed  | [CRO Group Conversation](/handbook/sales/)  | Michael McBride  |
| Four  | Thur  | [People](/handbook/people-group/)  | Carol Teskey and Dave Gilbert  |
| Four  | Fri  | Open  | Open  |
| Five  | Mon  | [Product](/handbook/product/)   | Scott Williamson  |
| Five  | Tue  | [CI/CD Section](/handbook/product/categories/#cicd-section)  | Jason Yavorska  |
| Five  | Wed  | [Strategic Marketing](/handbook/marketing/product-marketing/)  | Ashish Kuthiala  |
| Five  | Thur  | [Infrastructure](/handbook/engineering/infrastructure/)  | Gerir Lopez-Fernandez  |
| Five  | Fri  | [UX Research](/handbook/engineering/ux/ux-research/)  | Sarah O'Donnell  |
| Six  | Mon  | [Security](/handbook/engineering/security/)  | Jan Urbanc  |
| Six  | Tue  | [Meltano](/handbook/meltano/)  | Danielle Morrill  |
| Six  | Wed  | [Alliances](/handbook/alliances/)  | Brandon Jung  |
| Six  | Thur  | [Community Relations](/handbook/marketing/community-relations/)  | David Planella  |
| Six  | Fri  | Open  | Open  |


## Agenda

A possible agenda for the call is:

1. Accomplishments
1. Concerns
1. Stalled/need help
1. Plans
1. Questions and discussions live or in chat
