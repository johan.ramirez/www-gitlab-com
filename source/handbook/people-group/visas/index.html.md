---
layout: handbook-page-toc
title: "Visas"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Although this page is all about Visa's and how to apply for them, kindly keep in mind that GitLab does not  offer any form of Work Sponsorship anywhere in the world. We do have guidance below on how we provide sponsorship in the [Netherlands](/handbook/people-group/visas/#right-to-immigrate-to-the-netherlands), but we do not offer this in any other location at this time. We are also not in a position to transfer existing Work permit's at this time. 

## Expensing

You can expense:

- Cost of the VISA
- Cost of pictures for the VISA
- Cost of a lawyer to help you with the application
- Cost of postage
- Cost of applying for the [30% ruling](#30-ruling-in-the-netherlands)

You can _not_ expense relocation costs.

## Visa Contacts

For assistance with any employment or relocation visa please reach out to the People Ops Manager. Point of contact for Contribute and travel visas is TBD.

## Travel Visas

### Arranging a visa for travel <a name="arrange-visa"></a>

In some cases when traveling to a conference or summit you might need to arrange a visa to enter that specific country.

Always **check the website of the Embassy of the Country you want to visit** located in your home country. They will have the most updated information on whether or not you need a visa and what is needed for your application. In some instances you can travel under a visa waiver program (e.g. [ESTA](https://esta.cbp.dhs.gov/esta/)) in other cases you might need to arrange a tourist or business visa. Read their website carefully and ask People Ops Specialists if you need any help with acquiring the needed documents for your application.

We gladly organize and pay for local legal assistance, especially if you are at high risk of having a long process.

#### Tips & Tricks for visa arrangements <a name="visa-tips"></a>

- **Make sure to start on time with your arrangements for a visa**. It can take up to 3-4 **months** to acquire a US visa, as an example. Consider this recent example that several team members faced when they were arranging their visa to come to the summit in Austin in May of 2016. First, there was a delay of weeks to a month between the time they called to make an appointment at a US embassy and the date of the actual appointment. In some cases, the application then moved through the process with no issues and the visa was ready within two weeks. In other cases, the visa application was marked for "administrative processing" which can take up to 60 days, and is pretty much a [black hole in terms of information and process](https://www.quora.com/How-do-I-expedite-the-administrative-process-for-the-U-S-visa).
- Check if your passport is still valid long enough.
- Give priority to arranging your visa, it can take time to get an appointment to apply at the Embassy.
- Double check if you have all needed document and information. Ask People Ops Specialists to help you check this if you like.
- Think of the answers you will give during your visa interview; it can influence your visa approval/denial.
- Once the dates for a conference or summit are set start asap with your application if you have needed a
special visa in previous situations, just to give yourself enough time and room for errors if they arise.

##### Timeline guide for visa applications

_Please note that the below timeline is just a guide, and you should always check the guidance on the official website of the embassy for the country you are applying to before making your application or traveling to an interview._

- **6 months beforehand**: make appointment at embassy, and start gathering all necessary paperwork, forms, certificates, pictures, etc.
   - Sometimes you might call the embassy, and be told that you do not need to schedule an interview so far ahead of your trip. Don't take their word for it, since that advice is typically made with the "typical, best case"  process in mind and for example in the case of the USA does not account for the possibility of a black hole "administrative processing" period. In other words, make sure you get an appointment as early as possible.
   - If the country where the embassy/consulate is located where you are going for your application is listed as anything other than "High Income" by the [World Bank's "Little Data Book"](https://openknowledge.worldbank.org/bitstream/handle/10986/23968/9781464808340.pdf?sequence=4&isAllowed=y) then it is likely that the embassy/consulate there has a [high tendency to say "no" to visa applications](http://www.alllaw.com/articles/nolo/us-immigration/harder-get-visa-from-certain-countries.html). We recommend to engage the services of a local lawyer who is familiar with the embassy process to help check that your paperwork is in order, and to practice interview questions. You're also welcome to ask for support when you are in a "High Income" country. People Ops can help you with this.
- **3 months beforehand** (or earlier if indicated by the embassy you are applying to): go to embassy for interview.
- **2 months beforehand**: if you have not received your visa by now, contact your local lawyer again, and/or People Ops, to see if anything can be done to expedite the process.

### Prague Contribute 2020 Visa Invitation Letter

If you need a visa to travel to Czech Republic, please fill out this [form](https://docs.google.com/a/gitlab.com/forms/d/1hqwakBUfV5uybwtYHnRvkT_ZTm1CEUKqdNmU6e-BPdQ/edit). People Ops Specialist will verify accuracy and send you an email with the signed letter. If a GitLab team member would like to request a letter for their registered guest, GitLab can provide a short letter stating that we encourage guests to attend our conference. Please email People Ops at peopleops@domain with the full name and passport numbers of your guest to request for this letter. 

Once a request is sent, People Ops Specialist please use the `Template Visa Letter for Significant Others - GitLab` in the `Prague Visa Letter` shared folder to complete the letter, convert it to a pdf and forward it to the requestor.

If you need a letter confirming that you are covered under the GitLab Business Travel Accident Insurance Policy, please fill [Insurance form](https://forms.gle/QKb8prrtncYfnHFU9).If you need proof of accommodation, please fill the [Proof of Accomodation Form](https://forms.gle/nkAuN9MiVXfpAJHW6). A confirmation letter will be generated and forwarded to your email. If you don't receive it 5 mins after submitting, let People Ops know in #peopleops channel in Slack.

## Dutch Work Permits

Some of our GitLab team-members in the Netherlands have a "werkvergunning" or work permit under the [highly skilled migrants](https://ind.nl/en/work/Pages/Highly-skilled-migrant.aspx) category of the Immigration and Naturalization Service (IND).

- GitLab is a recognized organization ("erkend referent") with the IND, and Savvy provides support with respect to applying for new visas / permits or extending existing ones.
- Work permits must be renewed at the end of each contract period, but at minimum once every 5 years.
- At the time of applying for permit renewal, the application must satisfy various criteria including an age-dependent [minimum salary requirement](https://ind.nl/en/Pages/required-amounts-income_requirement.aspx#Application_residence_permit_highly_skilled_migrant_and_EU_blue_card) (with a step at age 30, also see this [handy table](https://ind.nl/en/Documents/Which_amount_applies_to_my_highly_skilled_migrant.pdf)). This requirement should be taken into consideration when issuing a new contract, since the contract can be made valid for just a year or for an indefinite period; thus triggering more or less frequent re-applications for work permit extensions. Permit extensions cannot be applied for when the current permit is still valid for more than 6 months.

Here is a [generally but not authoritative helpful guide](http://www.expatica.com/nl/visas-and-permits/When-your-residence-permit-expires-or-you-want-to-leave-the-Netherlands_108416.html) on considerations around permit extensions.

#### 30% Ruling in the Netherlands

The [30% reimbursement ruling](https://www.iamsterdam.com/en/living/take-care-of-official-matters/highly-skilled-migrants/thirty-percent-ruling) (better known as the 30% ruling) is a tax advantage for highly skilled migrants moving to the Netherlands. This benefit, granted for five years, allows them to receive 30% of their employment income tax free. As an example, when your employment income is EUR 60,000; the 30% ruling ensures a net pay of EUR 18,000. The remainder of EUR 42,000 is subject to Dutch taxes. In addition, when being entitled to the 30% ruling you can exclude your savings from Dutch income tax (which can be beneficial once you have exercised your stock options).

The 30% ruling is a mutual application filed by the employee and GitLab BV as the employer. During the onboarding procedure a [questionnaire](https://docs.google.com/document/d/1Ok6LS9T4P6tnPu2N6BDRDeveOYzd1ILpkbQRhl911w4/edit?ts=5caf1bca) is shared in order to gather the necessary information to file the application for the 30% ruling. People Ops shares the 30% ruling questionnaire and supporting documentation with  HRSavvy (the company that supports GitLab with visas and payroll in the Netherlands). HR Savvy will subsequently apply for the 30% ruling. There are some conditions to be satisfied to be granted the 30% ruling.

1. The employee has to be hired as an employee.
1. The employer and employee have to agree in writing that the 30% ruling is applicable (addendum to the employment agreement).
1. The employee has to transfer or to be recruited abroad by a Dutch employer. In two years before being hired by a Dutch employer, the employee must be living outside of the Netherlands for more than 16 months, at a minimal distance of 150 kilometers from the Dutch border.
1. The employee must have specific experience or expertise that is not or rarely available in the Netherlands.
1. The gross annual salary has to surpass a minimum (adjusted annually).

The decision from the Dutch Tax Authorities can take up to three months. Once your 30% ruling is granted, the application will be made retroactively in the payroll administration to your starting date. Read more from the Dutch tax authorities by clicking this [link](https://www.belastingdienst.nl/wps/wcm/connect/bldcontenten/belastingdienst/individuals/living_and_working/working_in_another_country_temporarily/you_are_coming_to_work_in_the_netherlands/30_facility_for_incoming_employees/).

#### Transferring the 30% Ruling from a Previous Employer

The 30% ruling is a tax advantage granted for five years, which means that you can carry this over to a new employer if your new role still fulfills the requirements of the 30% ruling. Note that you are only allowed to transfer the 30% ruling when there is a gap of maximum three months between your previous employment and your employment at GitLab BV. The 30% ruling questionnaire includes a section where you can state that you have already been entitled to the 30% ruling. Transferring the 30% ruling tends to be faster since your qualifications were already assessed at your previous employment.

#### BSN Number

A [BSN number](https://www.iamsterdam.com/en/living/take-care-of-official-matters/registration/citizen-service-number) is like a citizen number. It is required so new team members can be added to the B.V. Netherlands payroll. It's also required for things like health insurance and opening a bank account.

There are two options to get a BSN number in the Netherlands:

1. Getting a BSN number at the municipality. This process will take at least 1-4 weeks. There are no costs involved with registering and receiving a BSN at the municipality.

1. Getting a BSN number at your local [Expat center](https://www.iamexpat.nl/expat-info/organisations/expat-centres).
It is possible to make an appointment within 2 weeks.

### Right to Immigrate to the Netherlands

Everyone that meets the following requirements is welcome to move to the Netherlands (you will still need to pass the formal visa application process).
If you meet the requirements kindly read our [Relocation section](/handbook/people-group/code-of-conduct/#relocation) in our Code of Conduct, to ensure you get the right approval's. Email Peopleops@domain if you have any questions.
If you don't meet a requirement you have to file a request with your manager, please indicate clearly which requirements you meet and which ones you do not meet.

- When using the compensation calculator do you meet the Dutch salary requirement for [highly skilled migrants for 3 more years](https://ind.nl/en/work/working_in_the_netherlands/pages/highly-skilled-migrant.aspx) (be aware that the Dutch government bumps the requirement at 30, so if you're older than 27 you have to meet the higher norm).
- You have been a team member at GitLab for one year.
- You are not on a Performance Improvement Plan (PIP).

People Operations will then create a confidential issue in the Employment Issue Tracker with he following tasks to complete: 
1.  The Team member will need to complete this [form](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSekAouLh-DQsDXVho4TYL62CsBK8Pj0NhfI--npa3L1nF8IqA/viewform). This info is needed to get the team member's profile ready with our payroll provider in the Netherlands in order to get pay slips and other information. The team member will also need to provide a copy of their residence permit (if applicable), copy of their debit card, and passport. These can be send to People Operations separately.
2.  The Team member will also need to complete the [30% Ruling Questionnaire](https://docs.google.com/document/d/1Ok6LS9T4P6tnPu2N6BDRDeveOYzd1ILpkbQRhl911w4/edit?ts=5caf1bca) and send it to PeopleOps.
3.  The Team member will need to complete the [wage tax](https://drive.google.com/file/d/1q8N-idoYGFSCw2ajat9RscfQ004EL-PS/view) form and email it to People Operations once completed.
3.  If the team member does not have a BSN number they will need to apply for one asap. Details on that process can be found under the BSN number section on the [visas page](/handbook/people-group/visas/#bsn-number) in the handbook.
4.  PeopleOps will then send the payroll form, wage tax form and 30% ruling questionnaire to our payroll provider in the Netherlands along with the contract of employment, copy of the residence permit (if applicable), copy of the debit card, and passport. Then file the form in BambooHR. This email needs to be password protected.
5.  PeopleOPs will need to review if the position is in development or research, it likely qualifies for [WBSO (R&D tax credit)](/handbook/people-group/#wbso-rd-tax-credit-in-the-netherlands); add to the WBSO hour tracker and inform our WBSO consultant.
6.  Once the approved Visa and 30% ruling is received by the team member, the team members should email this to PeopleOps for filing in BambooHR.
6.  People Ops to [create an account](/handbook/general-onboarding/onboarding-processes/#add-expensify) for the new team member on Expensify.
7.  People Ops to update the compensation calculator.
8.  People Ops to update BambooHR with employee details
9.  People Ops to update Payroll

