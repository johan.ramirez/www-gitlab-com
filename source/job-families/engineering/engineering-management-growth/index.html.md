---
layout: job_family_page
title: "Engineering Management - Growth"
---

Managers in the engineering department at GitLab see the team as their product. While they are technically credible and know the details of what fullstack engineers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

## Engineering Manager, Growth

Engineering Managers in the Growth department own the delivery of features and improvements that aim to scale GitLab usage by connecting users to the existing value that GitLab already delivers.  The Growth department consists of two Engineering Managers, one responsible for Acquisition and Conversion, and one for Expansion and Retention.  They lead teams of fullstack engineers that work across the breadth of the GitLab project in order to make the necessary front end and back end changes to test hypothesis that drive improvement in user metrics. These teams work closely with their counterparts in the Product team to analyze the entire customer journey from acquisition of a customer, to the flow across multiple GitLab features, and even reactivation of lost users.

### Responsibilities
 - Hire an incredible team that lives our [values](/handbook/values/)
 - Improve the happiness and productivity of the team
 - Work on small features and bugs (nothing critical path)
 - Own Incident and Change Management, RCA, and Error Budget Management
 - Hold regular 1:1's with team members
 - Manage agile projects
 - Work across sections within engineering
 - Improve the quality, security and performance of the product

### Requirements
 - 2-5 years managing software engineering teams
 - Demonstrated teamwork in a peak performance organization
 - Experience working in Growth teams
 - Experience running a consumer scale platform
 - Product company experience
 - Enterprise software company experience
 - Computer science education or equivalent experience
 - Passionate about open source and developer tools
 - Exquisite communication skills
 - [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
 - Ability to use GitLab

### Nice-to-haves
 - Online community participation
 - Remote work experience
 - Startup experience
 - Significant open source contributions

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to complete a short written assessment.
* Next, candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 60 minute technical and behavioral interview with the Director of Engineering, Growth
* Candidates will then be invited to schedule a 60 minute interview with the Director of Product, Growth
* Candidates will then be invited to schedule a 60 minute Peer interview
* Candidates will then be invited to schedule a 50 minute interview with our Senior Director of Development
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](/handbook/hiring).
